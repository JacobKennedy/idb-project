import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.css';
import Header from "./Header";
import Slideshow from './slideshow';
import "./css/business-casual.css";
import Issues from "./Issues.js";
import Bills from "./Bills.js";
import CongressMembers from "./CongressMembers.js";
import About from "./About.js";
import Search from "./Search.js";
import ScrollToTop from "./ScrollToTop.js";
import ErrorPage from "./ErrorPage.js";
import Visualizations from "./Visualizations.js";

class App extends Component
{
  render()
  {
    return (
      <Router>
      <ScrollToTop>
      <div>
        <Header/>

        <Switch>
          <Route exact path = "/" component={Slideshow}/>
          <Route path = "/issues" component={Issues}/>
          <Route path = "/bills" component={Bills}/>
          <Route path = "/congressMembers" component={CongressMembers}/>
          <Route path = "/about" component={About}/>
          <Route path = "/visualizations" component={Visualizations}/>
          <Route path = "/search/:instance" component={Search}/>
          <Route path = "/404" component={ErrorPage}/>
          <Route path = "/:error" component={ErrorPage}/>
        </Switch>
        </div>
      </ScrollToTop>
      </Router>

    );
}
}

export default App;
