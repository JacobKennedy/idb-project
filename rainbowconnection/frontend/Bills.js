import React, { Component } from 'react';
import {Route, Link} from 'react-router-dom';
import './App.css';
import "./css/business-casual.css";
import Connections from "./Connections.js"
import { Chart } from "react-google-charts";
import ModelPage, { shortenDesc, title } from "./ModelPage.js";
import axios from 'axios';

const BILLSURL = "https://api.rainbowconnection.me/v1/bills";

//generate jsx to render a card with bill atrributes for instance
export function billCard (instance)
{
  return (
      <div className="modelColumn">

        <div className="modelIcon">
        <div className="modelInfo">
          <Link to={"/bills/" + instance.number}>
            <img className= "modelThumbnail" src={instance.icon} alt={"Picture of bill" + instance.number}/>
          </Link>
          {title(instance.name, shortenDesc(instance.name, 200))}
          <p><b>Bill #:</b> {instance.number}</p>
          <p><b>Introduced:</b> {instance.date}</p>
          <p><b>Sponsor:</b> {linkSponsor(instance.sponsor, instance.sponsor)}</p>
          <p><a href={instance.link}>Congress.gov link</a></p>
          </div>
          </div>

        </div>
  );
}

//link a single sponsor on a bill card, jsx for displayed name (used for highlighting)
export function linkSponsor (sponsorName, jsx)
{
  return(
    <Link to={"/congressMembers/" + sponsorName}>
      {jsx}
    </Link>
  );
}

//links all the cosponsors for a bill
export function linkCosponsors (cosponsors)
{
  let links = [];
  for (var i = 0; i < cosponsors.length; i++)
  {
    var listItem = cosponsors[i];
    if (i !== cosponsors.length - 1)
    {
      listItem += ", ";
    }

    links.push(
        <Link to={"/congressMembers/" + cosponsors[i]}>{listItem}</Link>
    );
  }
  return links;
}

export function billDescStart (desc)
{
  if (desc === undefined)
  {
    return;
  }
  return desc.substring(desc.indexOf("This"), desc.length);
}

class Bills extends Component {
  constructor(props) {
    super(props);
    this.state = {
        error: null,
        isLoaded: false,
    };
  }

  render() {
    return (
      <div>
      <Route path={`${this.props.match.path}/:instance`} render = {(props) => <BillInstance {...props}/>} />
      <Route exact path={this.props.match.path} render = {(props) => <ModelPage {...props} model = "bills" showSearch = {this.props.showSearch} searchValue = {this.props.searchValue}/>} />
      </div>
    );
  }
}

class BillInstance extends Component
{
  constructor(props) {
      super(props);
      this.state = {
          isLoaded : false,
          instance : {}
      };
  }

  async componentDidMount()
  {
    await axios.get(BILLSURL + "?name=" + this.props.match.params.instance).then(
      result => {

        //if the data exists, get it from the api
        if (result.data.length === 1)
        {
          let data = result.data[0];
          let instance = this.state.instance;

          instance.name = data.name;

          instance.number = data.num;
          if (instance.number === undefined)
          {
            instance.number = data.number;
          }
          instance.date = data.date;
          if (instance.date === undefined)
          {
            instance.date = data.intro_date;
          }
          instance.sponsor = data.sponsor;
          instance.summary = data.summary;
          instance.cosponsors = data.cosponsors;
          instance.link = data.link;
          instance.picture = "http://www.investvanuatu.org/wp/wp-content/uploads/2019/01/icoinstance.png";
          //instance.partyData = data.partyData;

          //alert(data.partydata);
          if (data.partydata !== undefined)
          {

            instance.partyData = [['Party', 'Number of Sponsors'],
            ['Democrats', data.partydata["Democrat"]],
            ['Republicans', data.partydata["Republican"]],
            ['Independents', data.partydata["Independent"]]];
          }



          /*instance.partyData = [['Party', 'Number of Sponsors'],
                                ['Democrats', data.d],
                                ['Republicans', data.r],
                                ['Independents', data.i]];*/

          instance.connections = [{ model: "issues",
                                    name:  data.connections[0],
                                    icon:  data.connections[1]},
                                  { model: "congressMembers",
                                    name:  data.connections[2],
                                    icon:  data.connections[3] + ".jpg"}];

          this.setState({
            isLoaded : true,
            instance : instance,
          });
        }
        else
        {
          //go to 404 page because we can't find the instance in the api
          this.props.history.push('/404');
        }

      },

      error => {
        //alert(error);

        this.setState({
          isLoaded : true,
          error
        });
      }
    );

  }

  billPage ()
  {
    let bill = this.state.instance;
    if (!this.state.isLoaded)
    {
      return (<p>Loading...</p>);
    }

    return (
        <div>
        <div className ="pageBox">
        <br/>
        <h2><b>{bill.name}</b></h2>
        <h2>{bill.number}</h2>
        <h2>Introduced: {bill.date}</h2>
        <h2>Sponsor: {bill.sponsor}</h2>
        <br/>
        <h3><b>Summary:</b> {billDescStart(bill.summary)}</h3>
        <br/>

        <div className ="billDisplay">
        <div className="row">
          <div className="column">
          <Chart
              width={'550px'}
              height={'400px'}
              chartType="PieChart"
              loader={<div>Loading Chart</div>}
              data={bill.partyData}
              options={{
                title: 'Sponsors by Party',
                backgroundColor: 'transparent',
              }}
            />
          </div>
          <div className="column">
            <h3><b><a href={bill.link}>Congress.gov link</a></b> </h3>
          <h3><b>Cosponsors:</b> {linkCosponsors(bill.cosponsors)}</h3>
          </div>
        </div>

        </div>
        </div>

        <Connections connections = {bill.connections} model = "bill"/>
        </div>
    );
  }

  render()
  {
    return (
      <div>
        <h3>{this.billPage()}</h3>

      </div>
    );
  }
}

export default Bills;
