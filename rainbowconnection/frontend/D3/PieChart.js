import React, { Component } from 'react';
import * as d3 from "d3";
import d3Tip  from "d3-tip";

import "./css/BarChart.css";

//This code is from D3 Pie Chart on codepen: https://codepen.io/darrengriffith/pen/KpKXZP

export function getTotalEvents (data)
{
  var total = 0;
  for (var i = 0; i < data.length; i++)
  {
    total += data[i].value;
  }
  return total;
}

class PieChart extends Component
{
  constructor(props) {
    super(props);
    this.state =
    {
      isLoaded: false,
    };
  }

  drawPieChart()
  {
        // Define SVG attributes
    var width = 400,
        height = 400,
        radius = 200;
      //alert("eventbrite found from prop" + this.props.data[0].value);
    // Use predefined D3 palette
    var colors = d3.scale.category20c()

    var totalValue = getTotalEvents(this.props.data);

    // Create a D3 layout that creates SVG arcs from the supplied data
    var pieLayout = d3.layout.pie()
    	.value(function(d) {
        return d.value;
      })

    // Create a D3 arc of the specified radius
    var arc = d3.svg.arc()
    	.outerRadius(radius)

    // Create the SVG graphic
    var svg = d3.select("pieChart").append('svg')
    	.attr('width', width)
    	.attr('height', height)


    const tip = d3Tip()
      .attr('class', 'd3-tip')
      .offset([-13, 0])
      .html(function(d) {
        return d.data.label + ": <span style='color:orangered'>" + Math.floor((d.data.value / totalValue) * 100) + "%</span>";
      });
    svg.call(tip);

    // Create a group to hold the chart
    svg.append('g')
      // Move the center of the chart to the center of the SVG
      .attr('transform', 'translate(' + (width - radius) + ', ' + (height - radius) + ')')
      // Select the complement of paths relative to the data
      .selectAll('path').data(pieLayout(this.props.data)).enter()
        // Add a group for each pie slice
        .append('g')
        .attr('class', 'slice')
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);

    // Add the pie slices
    d3.selectAll('g.slice')
    			.append('path')
          .attr('fill', function(d, i) {
            return colors(i);
          })
    			.attr('d', arc)


    // Add the slice labels
    d3.selectAll('g.slice')
    			.append('text')
    			.text(function(d, i) {
      			return d.data.label + ' (' + d.data.value + ')';
    			})
      		.attr('font-size', '12')
    			.attr('text-anchor', 'middle')
    			.attr('fill', 'black')
    			.attr('transform', function(d){
            // Move the lable to the center of its slice
            d.innerRadius = 0;
            d.outerRadius = radius;
            return 'translate(' + arc.centroid(d) + ')'
          })

  }

  componentDidUpdate()
  {
    if (this.props.isLoaded && !this.state.isLoaded)
    {
      this.drawPieChart();

      this.setState({
        isLoaded : true
      });
    }
  }

  render()
  {
    return (
      <div>

      <pieChart/>
        <br/>
      </div>
    );
  }
}

export default PieChart;
