import React, { Component } from 'react';
import {Link, withRouter} from 'react-router-dom';
import "./css/business-casual.css";

//the search bar was implemented using the Forms react tutorial: https://reactjs.org/docs/forms.html
class Header extends Component
{
  constructor(props) {
    super(props);
    this.state =
    {
      searchValue: "",
    };
    this.handleSeachChange = this.handleSeachChange.bind(this);
    this.handleSearchSubmit = this.handleSeachSubmit.bind(this);
  }

  //event that sets the search value when someone types in the search bar
  handleSeachChange (event)
  {
    this.setState({searchValue: event.target.value});
  }

  //event that happens when someone submits a search (presses enter in search form)
  handleSeachSubmit(event)
  {
    //alert('Search for: ' + this.state.searchValue);
    if (this.state.searchValue !== "")
    {
      this.props.history.push('/search/' + this.state.searchValue);
    }

    event.preventDefault();
  }

  getShortenedPath ()
  {
    let path = this.props.location.pathname.slice(1);
    var slash = path.indexOf("/");
    if (slash > 0)
    {
      path = path.substring(0, slash);
    }
    return path;
  }

  navHighlight (check)
  {
    //adds "active" to navbar title if this is the active page
    if (this.getShortenedPath() === check)
    {
      return "nav-item active px-lg-4";
    }
    return "nav-item px-lg-4";
  }

  render()
  {
    let path = this.getShortenedPath();

    //text for the title page
    if (path === "")
    {
      path = "Rainbow Connection";
    }
    else if (path === "congressMembers")
    {
      path = "Members of Congress";
    }

    return (
      <div>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"/>

        <h1 className="site-heading text-center text-white d-none d-lg-block">
          <span className="site-heading-lower">{path}</span>
        </h1>

        <nav className="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
          <div className="container">
            <a className="navbar-brand text-uppercase text-expanded text-white font-weight-bold d-lg-none" href="/">Rainbow Connection</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav mx-auto">
                <li className={this.navHighlight("")}>
                  <Link to = "/" className="nav-link text-uppercase text-expanded">Home</Link>
                </li>
                <li className={this.navHighlight("issues")}>
                  <Link to = "/issues" className="nav-link text-uppercase text-expanded">Issues</Link>
                </li>
                <li className={this.navHighlight("bills")}>
                  <Link to = "/bills" className="nav-link text-uppercase text-expanded">Bills</Link>
                </li>
                <li className={this.navHighlight("congressMembers")}>
                  <Link to = "/congressMembers" className="nav-link text-uppercase text-expanded">Members of Congress</Link>
                </li>
                <li className={this.navHighlight("visualizations")}>
                  <Link to = "/visualizations" className="nav-link text-uppercase text-expanded">Visualizations</Link>
                </li>
                <li className={this.navHighlight("about")}>
                  <Link to = "/about" className="nav-link text-uppercase text-expanded">About</Link>
                </li>
              </ul>

              <form className="form-inline mr-auto" onSubmit={this.handleSearchSubmit}>
              <div className="md-form my-0">
                <input
                  className="form-control transparent-input"
                  type="text"
                  placeholder="Search"
                  aria-label="Search"
                  value={this.state.searchValue}
                  onChange={this.handleSeachChange}
                />
              </div>
            </form>

            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(Header);
