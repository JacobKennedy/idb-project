import React, { Component } from 'react';
import {Route, Link} from 'react-router-dom';
import './App.css';
import "./css/business-casual.css";
import Connections from "./Connections.js";
import ModelPage, { shortenDesc } from "./ModelPage.js";
import axios from 'axios';
import { TwitterTimelineEmbed } from 'react-twitter-embed';

const CONGRESSURL = "https://api.rainbowconnection.me/v1/congress";

export function formatCommittees (c)
{
    var formatted = "";
    for (var i = 0; i < c.length - 1; i++)
    {
        formatted += c[i] + ", "
    }
    formatted += c[c.length - 1];
    return formatted;
}

//renders attributes for an instance of a congress member on a card
export function congressCard (instance)
{
  return (
      <div className="modelColumn">

        <div className="modelIcon">
        <div className="modelInfo">
        <Link to={"/congressMembers/" + instance.name}>
          <img className= "modelThumbnail" src={instance.icon} alt={instance.name}/>
        </Link>
        <h3><b>{shortenDesc(instance.name, 200)}</b></h3>
        <p>{instance.title}</p>
        <p><b>Party:</b> {instance.party}</p>
        <p><b>State:</b> {instance.state}</p>
        <p><b>Term End:</b> {instance.termEnd}</p>
        </div>
        </div>
      </div>
  );
}

//Returns the bool from string we get from the json
export function getBoolfromString (str)
{
  if (str === "False")
  {
    return false;
  }
  return true;
}

//returns jsx for social media link, if there is one
export function createSocialMediaLink (username, link)
{
  if (username === null)
  {
    return <b>N/A</b>;
  }
  return (<b><a href={link}>{username}</a></b>);
}

class CongressMembers extends Component {
  constructor(props) {
    super(props);
    this.state = {
        error: null,
        isLoaded: false,
    };
  }

  render() {
    return (
      <div>
      <Route path={`${this.props.match.path}/:instance`} render = {(props) => <CongressInstance {...props} />} />
      <Route exact path={this.props.match.path} render = {(props) => <ModelPage {...props} model = "congressMembers" loaded = {this.state.isLoaded} showSearch = {this.props.showSearch} searchValue = {this.props.searchValue}/>}/>
      </div>
    );
  }
}

class CongressInstance extends Component
{
  constructor(props) {
      super(props);
      this.state = {
          isLoaded : false,
          instance : {}
      };
  }

  async componentDidMount()
  {
    await axios.get(CONGRESSURL + "?name=" + this.props.match.params.instance).then(
      result => {

        //if the data exists, get it from the api
        if (result.data.length === 1)
        {
          let data = result.data[0];
          let instance = this.state.instance;

          instance.name = data.name;
          instance.title = data.title;

          instance.party = data.party;

          instance.phone = data.phone;
          instance.office = data.office;
          instance.icon = "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/" + data.memberid + ".jpg";
          instance.contact = data.contact;
          if (instance.contact === undefined)
          {
            instance.contact = data.contact_form;
          }

          instance.termEnd = data.termend;
          if (instance.termEnd === undefined)
          {
            instance.termEnd = data.term_end;
          }
          instance.state = data.state;
          instance.map = "https://maps.googleapis.com/maps/api/staticmap?center=" + instance.state + "&zoom=5&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C" + instance.state + "&key=AIzaSyDkjJLWEtXMs8dSDfEMGUHyXh1a5GMs0d0";
          instance.committees = data.committees;
          instance.senatelink = data.senatelink;
          if (instance.senatelink === undefined)
          {
            instance.senatelink = data.url;
          }
          instance.twitter = data.twitter;
          instance.linktwitter = data.linktwitter;
          if (instance.linktwitter === undefined)
          {
            instance.linktwitter = "https://twitter.com/" + instance.twitter;
          }
          instance.facebook = data.facebook;
          instance.linkfacebook = data.linkfacebook;
          if (instance.linkfacebook === undefined)
          {
            instance.linkfacebook = "https://facebook.com/" + instance.facebook;
          }

          instance.connections = [{ model: "bills",
                                    name:  data.connections[0],
                                    support: getBoolfromString(data.connections[1]),
                                    icon:  "http://www.investvanuatu.org/wp/wp-content/uploads/2019/01/icon.png"},
                                  { model: "issues",
                                    name:  data.connections[3],
                                    support:  getBoolfromString(data.connections[4]),
                                    icon:  data.connections[5]}];

          this.setState({
            isLoaded : true,
            instance : instance,
          });
        }
        else
        {
          //go to 404 page because we can't find the instance in the api
          this.props.history.push('/404');
        }

      },

      error => {
        //alert(error);

        this.setState({
          isLoaded : true,
          error
        });
      }
    );

  }

  congressPage ()
  {
    let congress = this.state.instance;
    if (!this.state.isLoaded)
    {
      return (<p>Loading...</p>);
    }

    return (
      <div>
        <div className="pageBox" >
          <br/>
          <div  style= {{margin:"50px"}}>
          <div className="row">

          <div className="column">
            <img src={congress.icon} alt = "congress member"/>
            <p><b>{congress.name}</b></p>
            <p><b>Title:</b> {congress.title}</p>
            <p><b>Party:</b> {congress.party}</p>
            <p><b>State:</b> {congress.state}</p>
            <p><b>Term End:</b> {congress.termEnd}</p>
            <p><b>Active Committees:</b></p>
            <p>{ formatCommittees(congress.committees)}</p>
          </div>

          <div className="column">
          <img src={congress.map} className="memberMap" alt = "map"/>
          </div>
          </div>
          </div>
          </div>

          <div className ="pageBox" align="center">
          <br/>
          <h2><b>Contact</b></h2>
          <h3><b><a href={congress.contact}>Contact Form</a></b> </h3>
          <h3>Office: {congress.office}</h3>
          <h3>Phone: {congress.phone}</h3>
          <h3><b><a href={congress.senatelink}>View on senate.gov</a></b> </h3>
          <h3>Twitter: {createSocialMediaLink(congress.twitter, congress.linktwitter)}</h3>
          <h3>Facebook: {createSocialMediaLink(congress.facebook, congress.linkfacebook)}</h3>

          <br/>
          <TwitterTimelineEmbed
           sourceType="profile"
           screenName={congress.twitter}
           options={{width: 500, height: 500}}
          />
          <br/>
          </div>

          <Connections connections = {congress.connections} model = "congress"/>
      </div>
    );
  }

  render()
  {
    return (
      <div>
        <h3>{this.congressPage()}</h3>
      </div>
    );
  }
}

export default CongressMembers;
