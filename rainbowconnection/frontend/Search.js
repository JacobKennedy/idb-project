import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './App.css';
import "./css/business-casual.css";
import {Tabs, Tab} from "react-bootstrap";
import {title} from "./ModelPage";
import Issues from "./Issues.js";
import Bills from "./Bills.js";
import CongressMembers from "./CongressMembers.js";

//generates jsx for a card displaying attributes that show up in a search
export function searchCard (instance, model, searchValue)
{
  var linkName = instance.name;
  if (model === "bills")
  {
    linkName = instance.number;
  }
  //alert(instance.search);
  return (
      <div className="modelColumn">

        <div className="modelIcon">
        <div className="modelInfo">
          <Link to={"/" + model + "/" + linkName}>
            <img className= "modelThumbnail" src={instance.icon} alt={"Picture of bill" + linkName}/>
          </Link>
          {title(instance.name, highlightText(instance.name, searchValue))}
          {formatSearchAttributes(instance.search, searchValue)}
          </div>
          </div>

        </div>
  );
}

//returns a list with the attribute split before and after the :
export function splitAttribute (attribute)
{
  var newAttribute = [];
  var idx = attribute.indexOf(":");
  if (idx !== -1)
  {
    newAttribute.push(attribute.substring(0, idx + 1));
    newAttribute.push(attribute.substring(idx + 1));
  }
  return newAttribute;
}

//returns jsx with new lines between attributes
export function formatSearchAttributes(attributesList, searchValue)
{
    var formatted = [];
    if (attributesList !== undefined)
    {
      for (var i = 0; i < attributesList.length; i++)
      {
        var attribute = splitAttribute(attributesList[i]);
        formatted.push(<div><b>{attribute[0]}</b>{highlightText(attribute[1], searchValue)}</div>);
      }
    }
    return formatted;
}

//returns text with search value highlighted
//referenced code: https://stackoverflow.com/questions/45736760/highlight-searching-text-on-type-react
export function highlightText (completeText, searchValue)
{
  if (completeText === undefined)
  {
    completeText = "";
  }

  //make sure we are searching
  if (searchValue === "")
  {
    return completeText;
  }

  //check if searchvalue is in the given text
  let value = searchValue.toLowerCase();
  let compareText = completeText.toLowerCase();
  let idx = compareText.indexOf(value, 0);
  if (idx === -1)
  {
    return completeText;
  }

  //iterate over every found index of searchValue and highlight it
  var newText = [];
  var startIdx = 0;
  while (idx !== -1)
  {
    newText.push(completeText.substring(startIdx, idx));
    newText.push(<strong>{completeText.substring(idx, idx + value.length)}</strong>);
    startIdx = idx + 1;
    idx = compareText.indexOf(value, idx + 1);
  }
  newText.push(completeText.substring(startIdx + value.length - 1));
  return newText;
}

//uses tab example from https://codepen.io/tchaffee/pen/yMmywz
class Search extends Component
{
  constructor(props)
  {
    super();
    this.state =
    {
      activeTab: "issues"
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(selectedTab)
  {
    this.setState({
      activeTab: selectedTab
    });

    //alert("tab = " + selectedTab);
  }

  render()
  {
    return (
      <div>
        <br/>

        <Tabs className="tab" activeKey={this.state.activeTab} onSelect={this.handleSelect}>
          <Tab className="tabBackground" eventKey="issues" title="Issues">
            <br/>
            <Issues match = "issues" showSearch = "false" searchValue = {this.props.match.params.instance}/>
            <br/>
          </Tab>
          <Tab className="tabBackground" eventKey="bills" title="Bills">
            <br/>
            <Bills match = "bills" showSearch = "false" searchValue = {this.props.match.params.instance}/>
            <br/>
          </Tab>
          <Tab className="tabBackground" eventKey="congressmembers" title="Members of Congress">
            <br/>
            <CongressMembers match = "congressMembers" showSearch = "false" searchValue = {this.props.match.params.instance}/>
            <br/>
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default Search;
