import React, { Component } from 'react';
import {Tabs, Tab} from "react-bootstrap";
import axios from 'axios';
import AsterPlot from "./D3/AsterPlot.js";
import StateMap from "./D3/StateMap.js";
import BubbleChart from "./D3/BubbleChart.js";
import BarChart from "./D3/BarChart.js";
import PieChart from "./D3/PieChart.js";
import CalendarChart from "./D3/CalendarChart.js";
import {getStateFromAbbreviation } from "./ModelPageButtons.js";

import './App.css';
import "./css/business-casual.css";

//Searches through data and returns index of label if it exists, otherwise return -1
export function findDataIndex (data, label)
{
  var index = -1;
  for (var i = 0; i < data.length; i++)
  {
    if (data[i].label === label)
    {
      index = i;
      break;
    }
  }
  return index;
}

class Visualizations extends Component
{
  constructor(props)
  {
    super();
    this.state =
    {
      activeTab: "billsPerIssue", //"billsPerIssue",
      dogPieData: [
        {
          label: "Event",
         	value: 0
        },
        {
          label: "Meetup",
         	value: 0
        },
        {
          label: "National Park",
         	value: 0
        },
      ],
      dogPieDataLoaded: false,
      dogBarData: [
        {
          label: "labrador retriever",
          value: 0
        },
      ],
      dogBarDataLoaded: false,
      dogCalendarData: [{
        "date": "2019-02-01",
        "total": 2,
        "details": [{
          "name": "Project 1",
        }, {
          "name": "Project 2",
        },],
      },
    ],
      dogCalendarDataLoaded: false,
      mapData:
      {
      },
      mapDataLoaded: false,
      issueBarData: [],
      issueBarDataLoaded: false,

    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(selectedTab)
  {
    this.setState({
      activeTab: selectedTab
    });
  }

  //number of bills per issue visualization
  billsPerIssueBar ()
  {
    return (
      <div>
        <div className="pageBox" align="center">
          <br />
          <h2><b>Number of bills per issue</b></h2>
          <br />
          <BarChart data = {this.state.issueBarData} isLoaded = {this.state.issueBarDataLoaded} x = "Issue" y = "Number of Bills" label = "IssueBarChart"/>
          <br />
        </div>
      </div>
    );
  }

  //number of states protected for each issue in a visualization
  issuesNumStates ()
  {
    return (
      <div>
        <div className="pageBox" align="center">
        <br />
        <h2><b>Number of states protected per issue</b></h2>
        <br />
        <AsterPlot dataURL = "https://api.rainbowconnection.me/v1/issues/all"/>
        <br />
        </div>
      </div>
    );
  }

  //renders a state map with the percent of sponsors of bills per each state
  billsSupportedByState ()
  {
    return (
      <div>
        <div className="pageBox" align="center">
        <br />
        <h2><b>Percent of bills supported per state</b></h2>
        <br />
        <StateMap data = {this.state.mapData} isLoaded = {this.state.mapDataLoaded}/>
        <br />
        </div>
      </div>
    );
  }

  //visualization of the dog activities timeline
  dogActivitiesBar ()
  {
    return (
      <div>
        <div className="pageBox" align="center">
        <br />
        <h2><b>Dog Breeds</b></h2>
        <br />
        <BarChart data = {this.state.dogBarData} isLoaded = {this.state.dogBarDataLoaded}  x = "Breed" y = "Total Dogs" label = "dogActivitiesBar"/>
        <br />
        </div>
      </div>
    );
  }

  //visualization of the dog group's data
  dogBreedsPie ()
  {
    return (
      <div>
        <div className="pageBox" align="center">
        <br />
        <h2><b>Dog Friendly Activity Types </b></h2>
        <br />
        <br />
        <PieChart data = {this.state.dogPieData} isLoaded = {this.state.dogPieDataLoaded}/>
        <br />
        </div>
      </div>
    );
  }

  //visualization of the dog group's data
  dogActivityCalendar ()
  {
    return (
      <div>
        <div className="pageBox" align="center">
        <br />
        <h2><b>Dogs Friendly Activity Calendar</b></h2>
        <br />
        <CalendarChart data = {this.state.dogCalendarData} isLoaded = {this.state.dogCalendarDataLoaded}/>
        <br />
        </div>
      </div>
    );
  }

  setMapData (data)
  {
    let mapData = this.state.mapData;
    var keys = Object.keys(data);
    //alert("setMapData");
    for (let i = 0; i < keys.length; i++)
    {
      //alert(data[keys[i]]);
      var stateData = data[keys[i]];
      var state = getStateFromAbbreviation(keys[i]);
      //var item = {[state]: };

      mapData[state] = {senate: Math.floor((stateData[0] / stateData[1]) * 100), rep: Math.floor((stateData[2] / stateData[3]) * 100)};
    }
    return mapData;
  }

  setIssueBarChartData (data)
  {
    let barData = this.state.issueBarData;

    for (let i = 0; i < data.length; i++)
    {
      //alert(data.objects[i].breed);
      var issue = data[i].connections[0];

      if (issue === "Gender Marker Updates On Identification Documents")
      {
        issue = "Gender Marker";
      }

      var index = findDataIndex(barData, issue);
      if (index === -1)
      {
        //have to make a new breed block because this is a new one
        barData.push({label: issue, value: 1});
      }
      else
      {
        barData[index].value++;
      }
    }
    return barData;
  }

  setPieChartData (data)
  {
    let pieData = this.state.dogPieData;

    for (let i = 0; i < data.objects.length; i++)
    {
      if (data.objects[i].type === "eventbrite")
      {
        pieData[0].value++;
      }
      else if (data.objects[i].type === "meetup")
      {
        //alert("meetup");
        pieData[1].value++;
      }
      else if (data.objects[i].type === "park")
      {
        pieData[2].value++;
      }
    }
    return pieData;
  }

  setDogBarChartData (data)
  {
    let barData = this.state.dogBarData;

    for (let i = 0; i < data.objects.length; i++)
    {
      //alert(data.objects[i].breed);
      var breedIndex = findDataIndex(barData, data.objects[i].breed);
      if (breedIndex === -1)
      {
        //have to make a new breed block because this is a new one
        barData.push({label: data.objects[i].breed, value: 1});
      }
      else
      {
        barData[breedIndex].value++;
      }
    }
    return barData;
  }

  setCalendarData (data)
  {
    let calendarData = this.state.dogCalendarData;
    var lastDate = calendarData.length - 1;

    for (let i = 0; i < data.objects.length; i++)
    {
      var details = {"name": data.objects[i].name};
      if (data.objects[i].date === calendarData[lastDate].date)
      {
        calendarData[lastDate].details.push(details);
        calendarData[lastDate].total++;
      }
      else
      {
        //have to make a new date block because this is a new one
        calendarData.push({"date": data.objects[i].date, "total": 1, "details":[details,]});
        lastDate++;
      }
    }
    return calendarData;
  }

  async componentDidMount()
  {
    //issue bar data
    await axios.get("https://api.rainbowconnection.me/v1/bills/all").then(
      result => {
        let data = result.data;
        let barData = this.setIssueBarChartData(data);

        this.setState({
          issueBarData : barData,
          issueBarDataLoaded : true
        });
      },

      error => {
        this.setState({
          error
        });
      }
    );

    //map data
    await axios.get("https://api.rainbowconnection.me/v1/visualization/map").then(
      result => {
        let data = result.data;
        let mapData = this.setMapData(data);

        this.setState({
          mapData : mapData
        });
      },

      error => {
        this.setState({
          error
        });
      }
    );
    this.setState({
      mapDataLoaded : true
    });

    //pie chart data
    for (var page = 1; page < 8; page++)
    {
      await axios.get("https://api.findadogfor.me/api/activity?page="+ page).then(
        result => {
          let data = result.data;
          let pieData = this.setPieChartData(data);

          this.setState({
            dogPieData : pieData
          });
        },

        error => {
          this.setState({
            error
          });
        }
      );
  }
  this.setState({
    dogPieDataLoaded : true
  });



  //calendar chart data
  for (page = 1; page < 8; page++)
  {
    await axios.get("https://api.findadogfor.me/api//activity/query?sort=date&page="+ page).then(
      result => {
        let data = result.data;
        let calendarData = this.setCalendarData(data);

        this.setState({
          dogCalendarData : calendarData
        });
      },

      error => {
        this.setState({
          error
        });
      }
    );
  }
  this.setState({
    dogCalendarDataLoaded : true
  });

  //bar chart data
  for (page = 1; page < 154; page++)
  {
    await axios.get("https://api.findadogfor.me/api/dog?page="+ page).then(
      result => {
        let data = result.data;
        let barData = this.setDogBarChartData(data);

        this.setState({
          setDogBarChartData : barData
        });
      },

      error => {
        this.setState({
          error
        });
      }
    );
  }
  this.setState({
    dogBarDataLoaded : true
  });
}


  render ()
  {
    return (
      <div>
        <br/>
        <Tabs className="tab" activeKey={this.state.activeTab} onSelect={this.handleSelect}>
          <Tab className="tabBackground" eventKey="billsPerIssue" title="Number of bills per issue">
            <br/>
            {this.billsPerIssueBar()}
            <br/>
          </Tab>
          <Tab className="tabBackground" eventKey="issueNumStates" title="Number of states protected per issue">
            <br/>
            {this.issuesNumStates()}
            <br/>
          </Tab>
          <Tab className="tabBackground" eventKey="billsbyState" title="Percent of bills supported per state">
            <br/>
            {this.billsSupportedByState()}
            <br/>
          </Tab>
          <Tab className="tabBackground" eventKey="Dogs1" title="Dog Friendly Activities Pie Chart">
            <br/>
            {this.dogBreedsPie()}
            <br/>
          </Tab>
          <Tab className="tabBackground" eventKey="Dogs2" title="Dog Breeds Bar Chart">
            <br/>
            {this.dogActivitiesBar()}
            <br/>
          </Tab>
          <Tab className="tabBackground" eventKey="Dogs3" title="Dog Friendly Activity Calendar">
            <br/>
            {this.dogActivityCalendar()}
            <br/>
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default Visualizations;
