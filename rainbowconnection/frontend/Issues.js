import React, { Component } from 'react';
import {Route, Link} from 'react-router-dom';
import './App.css';
import "./css/business-casual.css";
import Connections from "./Connections.js";
import ModelPage, {shortenDesc, title } from "./ModelPage.js";
import axios from 'axios';

const ISSUESURL = "https://api.rainbowconnection.me/v1/issues";

//generates jsx to render card with issue attributes for instance
export function issueCard (instance)
{
  return (
      <div className="modelColumn">

        <div className="modelIcon">
        <div className="modelInfo">
        <Link to={"/issues/" + instance.name}>
          <img className= "modelThumbnail" src={instance.icon} alt={"Picture of " + instance.name}/>
        </Link>
        <h3><b>{title(instance.name, shortenDesc(instance.name, 200))}</b></h3>
        <p><b>Federal protection:</b> {instance.fedProtect}</p>
        <p><b>Date Since Protected:</b> {instance.date}</p>
        <p><b>Number of states protected:</b> {instance.numStates}</p>
        <p><b>Description:</b> {shortenDesc(instance.desc)}</p>
        </div>
        </div>

      </div>
  );
}

//generates jsx used to display issue map (some embedded, some just images)
export function issueMap (map)
{
  if (map.includes("embed"))
  {
    return (
        <div align="center" style= {{margin:"30px"}}>
             <iframe src={map}
             title = "map"
             width="760"
             height="700"
             frameBorder="0"
             seamless="seamless"
             scrolling="auto">
             </iframe>
         </div>
    );
  }
  return (
    <div>
        <img className="issueMap" src={map} alt="map"/>
    </div>
  )
}

class Issues extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
        };
    }

    render() {
        return (
          <div>

          <Route path={`${this.props.match.path}/:instance`} render = {(props) => <IssueInstance {...props}/>} />
          <Route exact path={this.props.match.path} render = {(props) => <ModelPage {...props} model = "issues" showSearch = {this.props.showSearch} searchValue = {this.props.searchValue}/>} />
          </div>
        );
    }
}

class IssueInstance extends Component
{
  constructor(props) {
      super(props);
      this.state = {
          isLoaded : false,
          instance : {}
      };
  }

  async componentDidMount()
  {
    await axios.get(ISSUESURL + "?name=" + this.props.match.params.instance).then(
      result => {

        //if the data exists, get it from the api
        if (result.data.length === 1)
        {
          let data = result.data[0];
          let instance = this.state.instance;

          instance.name = data.name;

          instance.fedProtect = data.fedProtect;
          if (instance.fedProtect === undefined)
          {
            instance.fedProtect = data.fedprotect;
          }
          instance.date = data.date;
          instance.numStates = data.numStates;
          instance.desc = data.desc;
          if (instance.desc === undefined)
          {
            instance.desc = data.des;
          }

          instance.icon = data.icon;
          if (instance.icon === undefined)
          {
            instance.icon = "https://www.algbtical.org/equality00.jpg";
          }
          instance.map = data.map;

          if (instance.name === "Employment")
          {
            instance.map = "https://gitlab.com/JacobKennedy/idb-project/raw/master/rainbowconnection/public/img/employment.png";
          }
          if (instance.name === "Housing")
          {
            instance.map = "https://gitlab.com/JacobKennedy/idb-project/raw/master/rainbowconnection/public/img/housing.png";
          }
          if (instance.name === "Medical Facilities")
          {
            instance.map = "https://gitlab.com/JacobKennedy/idb-project/raw/master/rainbowconnection/public/img/medical_facilities.png";
          }

          //instance.connections = data.connections;
          instance.connections = [{ model: "bills",
                                    name:  data.connections[0],
                                    icon:  "http://www.investvanuatu.org/wp/wp-content/uploads/2019/01/icon.png"},
                                  { model: "congressMembers",
                                    name:  data.connections[2],
                                    icon:  data.connections[3] + ".jpg"}];

          this.setState({
            isLoaded : true,
            instance : instance,
          });
        }
        else
        {
          //go to 404 page because we can't find the instance in the api
          this.props.history.push('/404');
        }

      },

      error => {
        //alert(error);

        this.setState({
          isLoaded : true,
          error
        });
      }
    );
  }

  issuePage ()
  {
    let issue = this.state.instance;
    if (!this.state.isLoaded)
    {
      return (<p>Loading...</p>);
    }

    return (
      <div>
        <div className ="pageBox">
        <br />
        <h2><b>{issue.name}</b></h2>
        <h2>Federal protection: {issue.fedProtect}</h2>
        <h2>Date Since Protected: {issue.date}</h2>
        <br />
        <h3><b>Description: </b> {issue.desc}</h3>
        <br />

        {issueMap(issue.map)}
        <br />
        </div>

        <Connections connections = {issue.connections} model = "issue"/>

      </div>
    );
  }

  render()
  {
    return (
      <div>
        <h3>{this.issuePage()}</h3>
      </div>
    );
  }
}

export default Issues;
