import React, { Component } from 'react';
import {Dropdown, DropdownButton, Button} from "react-bootstrap";

const sponsorList = [ "Alan Lowenthal", "Barbara Lee", "Benjamin Cardin", "Bob Casey", "Bonnie Watson Coleman",
                      "Brian Schatz", "Christopher Coons", "Cory Booker", "Dave Loebsack", "David Cicilline",
                      "Edward Markey", "Jared Polis", "Jeanne Shaheen", "Jeff Merkley", "John Conyers Jr.",
                      "John Lewis", "Joseph Kennedy III", "Josh Gottheimer", "Kirsten Gillibrand", "Linda S\u00e1nchez",
                      "Mark Pocan", "Patrick Leahy", "Patty Murray", "Rosa DeLauro", "Scott Taylor",
                      "Tammy Baldwin", "Ted Lieu", "Terri Sewell", "Tim Kaine"
                    ];

//state list from https://gist.github.com/tleen/6299431
const stateList = ['Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming'];

//state abbreviations from https://gist.github.com/JeffPaine/3083347
const stateAbbreviations = [  "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA",
                              "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
                              "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
                              "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
                              "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

//Returns the abbreviation for a given state
export function getStateAbbreviation (state)
{
  var abb = "";
  for (var i = 0; i < stateList.length; i++)
  {
    if (state === stateList[i])
    {
      abb = stateAbbreviations[i];
      break;
    }
  }
  return abb;
}

//Returns the state given the abbreviation
export function getStateFromAbbreviation (state)
{
  var result = "";
  for (var i = 0; i < stateAbbreviations.length; i++)
  {
    if (state === stateAbbreviations[i])
    {
      result = stateList[i];
      break;
    }
  }
  return result;
}

class ModelPageButtons extends Component
{
  constructor(props) {
    super(props);
    this.state =
    {
      searchValue: "",
    };
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  //event that sets the search value when someone types in the search bar
  handleSearchChange (event)
  {
    this.setState({searchValue: event.target.value});
  }

  //figures out what text to display on the filter button based on the current filterValue
  getFilterButtonText (buttonNum = 0)
  {
    var filter = this.props.filterValues;
    var buttonText = "";
    if (this.props.model !== undefined  && filter[buttonNum] === "")
    {
      if (this.props.model === "issues")
      {
        buttonText = "Filter by Protection";
      }
      else
      if (this.props.model === "bills")
      {
        if (buttonNum === 0)
        {
          buttonText = "Filter by Chamber";
        }
        else if (buttonNum === 1)
        {
          buttonText = "Filter by Sponsor";
        }

      }
      else
      if (this.props.model === "congressMembers")
      {
        if (buttonNum === 0)
        {
          buttonText = "Filter by State";
        }
        else if (buttonNum === 1)
        {
          buttonText = "Filter by Party";
        }
        else if (buttonNum === 2)
        {
          buttonText = "Filter by Term End";
        }
      }
    }
    else
    {
      buttonText = "Filter: " + filter[buttonNum];
    }

    return buttonText;
  }

  //figures out what text to display on the sort button based on the current sortValue
  getSortButtonText ()
  {
    var filter = this.props.sortValue;
    var buttonText = "Sort";
    if (filter !== "")
    {
      buttonText = "Sort:" + filter;
    }

    return buttonText;
  }

  //gets the default search text for each model
  getDefaultSearchText ()
  {
    var searchText = "Search";
    if (this.props.model !== undefined)
    {
      if (this.props.model === "issues")
      {
        searchText = "Search for an issue";
      }
      else
      if (this.props.model === "bills")
      {
        searchText = "Search for a bill";
      }
      else
      if (this.props.model === "congressMembers")
      {
        searchText = "Search for a member of congress";
      }
    }

    return searchText;
  }

  //generates a dropdown item with title and call to either sort or filter based on var type
  formatDropdownItem (title, type, buttonNum = 0)
  {
    if (type === "filter")
    {
      return (<Dropdown.Item as="button" onSelect={() => this.props.filterHandler(title, buttonNum)}>{title}</Dropdown.Item>);
    }
    return (<Dropdown.Item as="button" onSelect={() => this.props.sortHandler(title)}>{title}</Dropdown.Item>);
  }

  //generates dropdown items for every element in itemList
  generateDropdownItems (itemList, type, buttonNum = 0)
  {
    let items = [];
    for (var i = 0; i < itemList.length; i++)
    {
      items.push(this.formatDropdownItem(itemList[i], type, buttonNum));
    }
    return items;
  }

  //returns UI for hardcoded dropdown options for filtering
  filterButton ()
  {
    var buttons = [];
    var dropdownItems = [];

    if (this.props.model === "issues")
    {
      dropdownItems.push(this.formatDropdownItem("Protected", "filter"));
      dropdownItems.push(this.formatDropdownItem("Not Protected", "filter"));
      buttons.push(
        <li className = "nav-item px-lg-4">
          <DropdownButton
            id="dropdown-item-button"
            title={this.getFilterButtonText()}
            variant= "secondary"
          >
            {dropdownItems}
          </DropdownButton>
        </li>
      );
    }
    else
    if (this.props.model === "bills")
    {
      dropdownItems.push(this.formatDropdownItem("House", "filter"));
      dropdownItems.push(this.formatDropdownItem("Senate", "filter"));

      buttons.push(
        <li className = "nav-item px-lg-4">
          <DropdownButton
            id="dropdown-item-button"
            title={this.getFilterButtonText(0)}
            variant= "secondary"
          >
            {dropdownItems}
          </DropdownButton>
        </li>
      );

      dropdownItems = [];
      dropdownItems.push(
        <Dropdown className="dropdown-menu-scroll">
          {this.generateDropdownItems(sponsorList, "filter", 1)}
        </Dropdown>
      );

      buttons.push(
        <li className = "nav-item px-lg-4">
          <DropdownButton
            id="dropdown-item-button"
            title={this.getFilterButtonText(1)}
            variant= "secondary"
          >
            {dropdownItems}
          </DropdownButton>
        </li>
      );
    }
    else
    if (this.props.model === "congressMembers")
    {
      dropdownItems.push(
        <Dropdown className="dropdown-menu-scroll">
          {this.generateDropdownItems(stateList, "filter")}
        </Dropdown>
      );

      buttons.push(
      <li className = "nav-item px-lg-4">
        <DropdownButton
          id="dropdown-item-button"
          title={this.getFilterButtonText(0)}
          variant= "secondary"
        >
          {dropdownItems}
        </DropdownButton>
      </li>
      );

      dropdownItems = [];
      dropdownItems.push(this.formatDropdownItem("Democrat", "filter", 1));
      dropdownItems.push(this.formatDropdownItem("Republican", "filter", 1));
      dropdownItems.push(this.formatDropdownItem("Independent", "filter", 1));

      buttons.push(
      <li className = "nav-item px-lg-4">
        <DropdownButton
          id="dropdown-item-button"
          title={this.getFilterButtonText(1)}
          variant= "secondary"
        >
          {dropdownItems}
        </DropdownButton>
      </li>
      );

      dropdownItems = [];
      dropdownItems.push(this.formatDropdownItem("2019", "filter", 2));
      dropdownItems.push(this.formatDropdownItem("2021", "filter", 2));
      dropdownItems.push(this.formatDropdownItem("2023", "filter", 2));

      buttons.push(
      <li className = "nav-item px-lg-4">
        <DropdownButton
          id="dropdown-item-button"
          title={this.getFilterButtonText(2)}
          variant= "secondary"
        >
          {dropdownItems}
        </DropdownButton>
      </li>
      );
    }

    return buttons;
  }

  //returns UI for hardcoded dropdown options for sorting
  sortButton ()
  {
    let button = [];


    if (this.props.model === "issues")
    {
      button.push (this.formatDropdownItem("Name (A-Z)", "sort"));
      button.push (this.formatDropdownItem("Name (Z-A)", "sort"));
      button.push (this.formatDropdownItem("Number of States protected (increasing)", "sort"));
      button.push (this.formatDropdownItem("Number of States protected (decreasing)", "sort"));
    }
    else
    if (this.props.model === "bills")
    {
      button.push (this.formatDropdownItem("Date Introduced (increasing)", "sort"));
      button.push (this.formatDropdownItem("Date Introduced (decreasing)", "sort"));
    }
    else
    if (this.props.model === "congressMembers")
    {
      button.push (this.formatDropdownItem("Name (A-Z)", "sort"));
      button.push (this.formatDropdownItem("Name (Z-A)", "sort"));
      button.push (this.formatDropdownItem("Term End (increasing)", "sort"));
      button.push (this.formatDropdownItem("Term End (decreasing)", "sort"));
    }

    return (
      <li className = "nav-item px-lg-4">
        <DropdownButton
          id="dropdown-item-button"
          title={this.getSortButtonText()}
          variant= "secondary"
        >
          {button}
        </DropdownButton>
      </li>
    );
  }

  render ()
  {
    return (
      <div>
      <br/>
      <nav className="navbar navbar-expand-lg" id="searchnav">
        <div className="container">
          <a className="navbar-brand text-uppercase text-expanded text-white font-weight-bold d-lg-none" href="/">Rainbow Connection</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav mx-auto">

            <li className = "nav-item px-lg-4">
              <form className="form-inline mr-auto" id={this.state.searchValue} onSubmit={this.props.searchSubmitHandler} >
                <div className="md-form my-0">
                  <input
                    className="form-control transparent-input"
                    type="text"
                    placeholder={this.getDefaultSearchText()}
                    aria-label="Search"
                    value={this.state.searchValue}
                    onChange={this.handleSearchChange}
                    style={{width:"300px"}}
                  />
                </div>
              </form>
            </li>

            {this.sortButton()}
            {this.filterButton()}

            <li className = "nav-item px-lg-4">
              <Button variant="secondary" onClick={this.props.resetHandler}>Reset</Button>
            </li>

            </ul>

          </div>
        </div>
      </nav>
        <br/>

      </div>
    );
  }
}

export default ModelPageButtons;
