import flask
import psycopg2
from flask import request, jsonify
from flask_cors import CORS, cross_origin
from connection import make_connection

app = flask.Flask(__name__)
cors = CORS(app)
app.config["DEBUG"] = True
app.config['CORS_HEADERS'] = 'Content-Type'

issues = []
try:
    connection = make_connection()

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM issues")

    row = cursor.fetchone()

    while row != None:

        name = row[0]
        if name == None:
            name = "N/A"

        fedProtect = row[6]
        if fedProtect == None:
            fedProtect = "N/A"

        date = row[1]
        if date == None:
            date = "N/A"

        numStates = row[7]
        if numStates == None:
            numStates = "N/A"

        desc = row[2]
        if desc == None:
            desc = "N/A"

        icon = row[3]
        if icon == None:
            icon = "N/A"

        map = row[4]
        if map == None:
            map = "N/A"

        bills = row[5]


        connections = [{'model': 'bills',
                        'name': bills[0],
                        'icon': 'default'},
                       {'model': "congressMembers",
                        'name': "Tammy Baldwin",
                        'icon': "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/B001230.jpg"}
        ]


        issues.append({'name': name, 'fedProtect': fedProtect, 'date': date, 'numStates': numStates, 'desc': desc, 'icon': icon, 'map': map, 'connections': connections, 'bills': bills})


        row = cursor.fetchone()



    cursor.close()

    connection.close()
except psycopg2.DatabaseError, e:
    if connection:
        connection.rollback()


bills = []
try:
    connection = make_connection()

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM bills")

    row = cursor.fetchone()

    while row != None:

        name = row[1]
        if name == None:
            name = "N/A"

        num = row[7]
        if num == None:
            num = "N/A"

        date = row[2]
        if date == None:
            date = "N/A"

        sponsor = row[3]
        if sponsor == None:
            sponsor = "N/A"

        link = row[6]
        if link == None:
            link = "N/A"

        summary = row[4]
        if summary == None:
            summary = "N/A"

        dems = row[9]
        reps = row[8]
        ind = row[10]

        partyData = []
        partyData += ['Democrats', dems]
        partyData += ['Republicans', reps]
        partyData += ['Independents', ind]

        cosponsors = row[5]

        issuename = "Error: issue not found"
        photo = "Error"
        for issue in issues:
            bs = issue.get('bills')
            if num in bs:
                issuename = issue.get('name')
                photo = issue.get('icon')
                break


        connections = [{'model': 'congressMembers',
                        'name': sponsor,
                        'icon': 'test.jpg'},
                       {'model': "issues",
                        'name': issuename,
                        'icon': photo}
        ]


        bills.append({'name': name, 'num': num, 'date': date, 'sponsor': sponsor, 'link': link, 'summary': summary,
                      'partyData': partyData, 'cosponsors': cosponsors, 'connections': connections, 'picture': 'default'})


        row = cursor.fetchone()



    cursor.close()

    connection.close()
except psycopg2.DatabaseError, e:
    if connection:
        connection.rollback()

congress = []

try:
    connection = make_connection()

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM congress")

    row = cursor.fetchone()

    while row != None:

        name = row[0]
        if name == None:
            name = "N/A"
        title = row[5]
        if title == None:
            title = "N/A"
        party = row[7]
        if party == None:
            party = "N/A"
        phone = row[9]
        if phone == None:
            phone = "N/A"
        office = row[8]
        if office == None:
            office = "N/A"
        contact = row[11]
        if contact == None:
            contact = "N/A"
        id = row[13]
        state = row[6]
        termEnd = "N/A"
        if title == "Representative":
            termEnd = "2019"
        if title == "Senator, 1st Class":
            termEnd = "2019"
        if title == "Senator, 2nd Class":
            termEnd = "2021"
        if title == "Senator, 3rd Class":
            termEnd = "2023"

        committees = row[12]

        link = row[1]

        twitter = row[2]
        linktwitter = "N/A"
        if twitter == None:
            twitter = "N/A"
        else:
            linktwitter = 'https://twitter.com/'+twitter+'?lang=en'

        facebook = row[3]
        linkfacebook = "N/A"
        if facebook == None:
            facebook = "N/A"
        else:
            linkfacebook = 'https://www.facebook.com/'+facebook+'/'

        linktwitter = 'https://twitter.com/'+twitter+'?lang=en'

        b = 'A Bill to amend the Voting Rights Act of 1965 to revise the criteria for determining which States and political subdivisions are subject to section 4 of the Act, and for other purposes'
        support = False

        i = "Hate Crimes"
        supporti = False

        photo = "http://blog.nohatespeechmovement.org/wp-content/uploads/2014/07/half-vic-lgbt.jpg"

        for bill in bills:
            csp = bill.get('cosponsors')
            if name in csp:
                b = bill.get('name')
                support = True
                i = bill.get('connections')[1].get('name')
                supporti = True
                photo = bill.get('connections')[1].get('icon')
                break


        connections = [{'model': 'bills',
                       'name': b,
                       'support': support,
                       'icon': 'default'},
                      {'model': "issues",
                       'name': i,
                       'support': supporti,
                       'icon': photo}
        ]


        toAdd = {'name': name, 'title': title, 'party': party, 'phone': phone, 'office': office, 'contact': contact,
                 'icon': 'https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/' + id +'.jpg',
                 'map': 'https://maps.googleapis.com/maps/api/staticmap?center=' + state + '&zoom=5&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C'+ state + '&key=AIzaSyDkjJLWEtXMs8dSDfEMGUHyXh1a5GMs0d0',
                 'state': state, 'termEnd': termEnd,'committess': committees,'senatelink': link,
                 'twitter': twitter, 'facebook': facebook, 'linktwitter': linktwitter, 'linkfacebook': linkfacebook, 'connections': connections}
        congress.append(toAdd)
        row = cursor.fetchone()



    cursor.close()

    connection.close()
except psycopg2.DatabaseError, e:
    if connection:
        connection.rollback()


#congress = [
#     {'name': Ted Cruz
#     'title': 'Senator, 1st',
#     'party': 'Republican',
#     'office': '404 Russel Senate Office Building'},
#    {'name': 'John Cornyn',
#     'title': 'Senator, 2nd Class',
#     'party': 'Republican',
#     'phone': '202-224-2934',
#     'office': '517 Hart Senate Office Building'},
#    {'name': 'Tammy Baldwin',
#     'title': 'Senator, 1st Class',
#     'party':'Democrat',
#     'phone': '202-224-5653',
#     'office': '709 Hart'}
#]



@app.route('/', methods=['GET'])
@cross_origin()
def home():
        return "<h1>Rainbow Connection API</h1><p>This is a prototype API for getting information about gay stuff.</p>"

@app.route('/api/v1/congress/all', methods=['GET'])
@cross_origin()
def congress_api_all():
    return jsonify(congress)

@app.route('/api/v1/congress', methods=['GET'])
@cross_origin()
def congress_api_id():

    if 'name' in request.args:
        name = request.args['name']
    else:
        return jsonify(congress)

    results = []

    for c in congress:
        if c['name'] == name:
            results.append(c)

    return jsonify(results)

#issues = [
#    {'name': 'Housing',
#     'fedProtect': 'No',
#     'date': 'N/A',
#     'numStates': '22',
#     'Description': 'The Office of Fair Housing and Equal Opportunity (FHEO) is an agency within the United States Department of Housing and Urban Development. FHEO is responsible for administering and enforcing federal fair housing laws and establishing policies that make sure all Americans have equal access to the housing of their choice. Housing discrimination refers to discrimination against potential or current tenants by landlords. In the United States, there is no federal law against #such discrimination on the basis of sexual orientation or gender identity, but at least twenty-two states and many major cities have enacted laws prohibiting it. See, for example, Washington House Bill 2661.'},
#    {'name': 'Medical Facilities',
#     'fedProtect': 'Yes',
#     'date': '4/14/2010',
#     'numStates': '50',
#     'desc': 'On April 14, 2010, President Barack Obama issued an Executive Order to the Department of Health and Human Services to draft new rules for all hospitals accepting Medicare or Medicaid funds. They would require facilities to grant visitation and medical decision-making rights to gay and lesbian partners, as well as designees of others such as widows and widowers. Such rights are not protected by law in many states. Obama said he was inspired by the case of a Florida family, where #one of the mothers died while her partner and four children were denied visitation by the hospital.'},
#    {'name': 'Employment',
#     'fedProtect': 'No',
#     'date':'N/A',
#     'numStates': '22',
#     'desc': 'There is no federal statute addressing employment discrimination based on sexual orientation or gender identity. Protections at the national level are limited. Some regulations protect government employees but do not extend their protections to the private sector. Twenty-two states, the District of Columbia, Puerto Rico, and over 140 cities and counties have enacted bans on discrimination based on sexual orientation and/or sexual identity. Employment discrimination refers to discriminatory employment practices such as bias in hiring, promotion, job assignment, termination, and compensation, and various types of harassment. In the United States there is "very little statutory, common law, and case law establishing employment discrimination based upon sexual orientation as a legal wrong.'}
#]

@app.route('/api/v1/issues/all', methods=['GET'])
@cross_origin()
def issues_api_all():
    return jsonify(issues)

@app.route('/api/v1/issues', methods=['GET'])
@cross_origin()
def issues_api_id():

    if 'name' in request.args:
        name = request.args['name']
    else:
        return "Error: No name field provided. Please specify an id."

    results = []


    for issue in issues:
        if issue['name'] == name:
            results.append(issue)

    return jsonify(results)

#bills = [
#    {'name': 'End Racial and Religious Profiling Act',
#     'number': 'S. 411',
#     'date': '2017-02-16',
#     'sponsor': 'Sen. Benjamin L. Cardin'},
#    {'name': 'A Bill to modernize laws and policies, and eliminate discrimination, with respect to people living with HIV/Aids, and for other purposes',
#     'number': 'S. 2186',
#     'date': '2017-12-04',
#     'sponsor': 'Christopher A. Coons'},
#    {'name': 'A Bill to amend the Voting Rights Act of 1965 to revise the criteria for determining which States and political subdivisions are subject to section 4 of the Act, and for other purposes',
#     'number': 'S. 1419',
#     'date':'2017-06-22',
#     'sponsor': 'Patrick J. Leahy'}
#]

@app.route('/api/v1/bills/all', methods=['GET'])
@cross_origin()
def bills_api_all():
    return jsonify(bills)

@app.route('/api/v1/bills', methods=['GET'])
@cross_origin()
def bills_api_id():

    if 'name' in request.args:
        name = request.args['name']
    else:
        return "Error: No name field provided. Please specify an id."


    results = []


    for bill in bills:
        if bill['name'] == name:
            results.append(bill)


    return jsonify(results)

app.run(host='0.0.0.0')
