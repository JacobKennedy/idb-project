import flask
import psycopg2
import psycopg2.extras

# import requests
# import json
from flask import request, jsonify
from flask_cors import CORS, cross_origin
from connection import make_connection

application = flask.Flask(__name__)
cors = CORS(application)
application.config["DEBUG"] = True
application.config["CORS_HEADERS"] = "Content-Type"

# Parameters for api requests
congressParams = ("phrase", "name", "title", "party", "state", "termend", "chamber")
issuesParams = ("phrase", "name", "fedProtect", "date", "numStates", "des")
billsParams = ("phrase", "num", "date", "sponsor")

# SQL statements used to query our database
congressSQLAll = "SELECT * FROM congress"
issuesSQLAll = "SELECT * FROM issues"
billsSQLAll = "SELECT * FROM bills"
congressSQL = (
    "SELECT * FROM congress WHERE name ILIKE %(name)s "
    "AND title ILIKE %(title)s "
    "AND party ILIKE %(party)s "
    "AND state ILIKE %(state)s "
    "AND termend ILIKE %(termend)s "
    "AND chamber ILIKE %(chamber)s "
    "AND (name ILIKE %(phrase)s "
    "OR title ILIKE %(phrase)s "
    "OR party ILIKE %(phrase)s "
    "OR state ILIKE %(phrase)s "
    "OR termend ILIKE %(phrase)s "
    "OR ARRAY_TO_STRING(committees, ',') ILIKE %(phrase)s "
    "OR office ILIKE %(phrase)s "
    "OR phone ILIKE %(phrase)s "
    "OR twitter ILIKE %(phrase)s "
    "OR chamber ILIKE %(phrase)s "
    "OR facebook ILIKE %(phrase)s) "
    "ORDER BY "
    "CASE WHEN %(desc)s = 'False' THEN "
    "CASE %(sort_by)s "
    "WHEN 'name' THEN name "
    "WHEN 'title' THEN title "
    "WHEN 'party' THEN party "
    "WHEN 'state' THEN state "
    "WHEN 'termend' THEN termend "
    "END "
    "END, "
    "CASE WHEN %(desc)s = 'True' THEN "
    "CASE %(sort_by)s "
    "WHEN 'name' THEN name "
    "WHEN 'title' THEN title "
    "WHEN 'party' THEN party "
    "WHEN 'state' THEN state "
    "WHEN 'termend' THEN termend "
    "END "
    "END DESC "
)
issuesSQL = (
    "SELECT * FROM issues WHERE name ILIKE %(name)s "
    "AND fedProtect ILIKE %(fedProtect)s "
    "AND date ILIKE %(date)s "
    "AND numStates ILIKE %(numStates)s "
    "AND des ILIKE %(des)s "
    "AND (name ILIKE %(phrase)s "
    "OR fedProtect ILIKE %(phrase)s "
    "OR date ILIKE %(phrase)s "
    # "OR numStates ILIKE %(phrase)s "
    "OR des ILIKE %(phrase)s) "
    "ORDER BY "
    "CASE WHEN %(desc)s = 'False' THEN "
    "CASE %(sort_by)s "
    "WHEN 'name' THEN name "
    "WHEN 'fedProtect' THEN fedProtect "
    "WHEN 'date' THEN date "
    "WHEN 'numStates' THEN numStates "
    "WHEN 'des' THEN des "
    "END "
    "END, "
    "CASE WHEN %(desc)s = 'True' THEN "
    "CASE %(sort_by)s "
    "WHEN 'name' THEN name "
    "WHEN 'fedProtect' THEN fedProtect "
    "WHEN 'date' THEN date "
    "WHEN 'numStates' THEN numStates "
    "WHEN 'des' THEN des "
    "END "
    "END DESC "
)
billsSQL = (
    "SELECT * FROM bills WHERE num ILIKE %(num)s "
    "AND date ILIKE %(date)s "
    "AND (sponsor ILIKE %(sponsor)s "
    "OR ARRAY_TO_STRING(cosponsors, ',') ILIKE %(sponsor)s) "
    "AND (num ILIKE %(phrase)s "
    "OR date ILIKE %(phrase)s "
    "OR name ILIKE %(phrase)s "
    "OR summary ILIKE %(phrase)s "
    "OR ARRAY_TO_STRING(cosponsors, ',') ILIKE %(phrase)s "
    "OR sponsor ILIKE %(phrase)s) "
    "ORDER BY "
    "CASE WHEN %(desc)s = 'False' THEN "
    "CASE %(sort_by)s "
    "WHEN 'num' THEN num "
    "WHEN 'date' THEN date "
    "WHEN 'sponsor' THEN sponsor "
    "END "
    "END, "
    "CASE WHEN %(desc)s = 'True' THEN "
    "CASE %(sort_by)s "
    "WHEN 'num' THEN num "
    "WHEN 'date' THEN date "
    "WHEN 'sponsor' THEN sponsor "
    "END "
    "END DESC "
)

# generated using map_visual.py. Hardcoded to reduce load time
mapData = {
    "AL": (4, 60, 5, 126),
    "AK": (2, 30, 2, 18),
    "AZ": (0, 45, 40, 180),
    "AR": (0, 30, 0, 72),
    "CA": (21, 30, 470, 972),
    "CO": (5, 30, 35, 126),
    "CT": (24, 30, 52, 90),
    "DE": (18, 30, 6, 18),
    "FL": (4, 30, 148, 486),
    "GA": (0, 30, 30, 270),
    "HI": (17, 30, 23, 36),
    "ID": (0, 30, 0, 36),
    "IL": (20, 30, 121, 324),
    "IN": (3, 30, 15, 162),
    "IA": (0, 30, 10, 72),
    "KS": (0, 30, 0, 90),
    "KY": (0, 30, 13, 108),
    "LA": (0, 30, 8, 108),
    "ME": (6, 30, 12, 36),
    "MD": (20, 30, 54, 144),
    "MA": (28, 30, 105, 162),
    "MI": (13, 30, 50, 270),
    "MN": (25, 45, 50, 144),
    "MS": (0, 45, 11, 72),
    "MO": (4, 30, 3, 144),
    "MT": (4, 30, 0, 36),
    "NE": (0, 30, 0, 54),
    "NV": (8, 30, 39, 72),
    "NH": (21, 30, 14, 36),
    "NJ": (24, 30, 95, 216),
    "NM": (12, 30, 9, 54),
    "NY": (14, 30, 233, 504),
    "NC": (0, 30, 28, 234),
    "ND": (4, 30, 0, 18),
    "OH": (11, 30, 52, 306),
    "OK": (0, 30, 0, 108),
    "OR": (24, 30, 45, 90),
    "PA": (8, 30, 68, 378),
    "RI": (14, 30, 29, 36),
    "SC": (0, 30, 2, 144),
    "SD": (0, 30, 0, 18),
    "TN": (0, 30, 25, 162),
    "TX": (0, 30, 75, 666),
    "UT": (0, 30, 0, 90),
    "VT": (19, 30, 11, 18),
    "VA": (10, 30, 37, 198),
    "WA": (22, 30, 82, 180),
    "WV": (0, 30, 0, 54),
    "WI": (13, 30, 37, 144),
    "WY": (0, 30, 0, 18),
}

# Creates dict of variables to be inputed into the SQL statement
def makeVars(args, params, id=None, sort_by="name", desc="False"):
    out = {}
    for param in params:
        if param in args:
            out[param] = "%" + args[param] + "%"
        else:
            out[param] = "%"

    if id != None:
        for k in id.keys():
            if id[k] != "%":
                out[k] = id[k]

    if "sort_by" in args:
        out["sort_by"] = args["sort_by"]
    else:
        out["sort_by"] = sort_by

    if "desc" in args:
        out["desc"] = args["desc"]
    else:
        out["desc"] = desc

    return out


# Execute a given SQL statement using a given dict of variables
def executeSQL(inSql, inVars=None):
    results = []
    try:
        connection = make_connection()
        cursor = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cursor.execute(inSql, vars=inVars)
        results = cursor.fetchall()

    except (psycopg2.DatabaseError):
        if connection:
            connection.rollback()

    return results


# uses api data to create URLs and include them in congress api calls
def addCongressURLs(results):
    for row in results:
        row["icon"] = (
            "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/"
            + row["memberid"]
            + ".jpg"
        )
        row["map"] = (
            "https://maps.googleapis.com/maps/api/staticmap?center="
            + row["state"]
            + "&zoom=5&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"
            + row["state"]
            + "&key=AIzaSyDkjJLWEtXMs8dSDfEMGUHyXh1a5GMs0d0"
        )
        if row["twitter"] != None:
            row["linktwitter"] = "https://twitter.com/" + row["twitter"] + "?lang=en"
        if row["facebook"] != None:
            row["linkfacebook"] = "https://www.facebook.com/" + row["facebook"]


# replace abreviated party names with full party names
def makePartyData(results):
    for row in results:
        partyData = {
            "Democrat": row["d"],
            "Republican": row["r"],
            "Independent": row["i"],
        }
        del row["d"]
        del row["r"]
        del row["i"]
        row["partydata"] = partyData


# API home page
@application.route("/", methods=["GET"])
@cross_origin()
def home():
    return "<h1>Rainbow Connection API</h1>"


# get all congress members
@application.route("/v1/congress/all", methods=["GET"])
@cross_origin()
def congress_api_all():

    results = executeSQL(congressSQLAll)
    addCongressURLs(results)
    return jsonify(results)


# get congress member by name
@application.route("/v1/congress", methods=["GET"])
@cross_origin()
def congress_api_id():

    if "name" in request.args:
        return congress_api_search(request.args["name"])

    return congress_api_all()


# get congress members, filtered by given parameters
@application.route("/v1/congress/search", methods=["GET"])
@cross_origin()
def congress_api_search(n="%"):
    results = executeSQL(
        congressSQL, makeVars(request.args.to_dict(), congressParams, {"name": n})
    )
    addCongressURLs(results)
    return jsonify(results)


# get all issues
@application.route("/v1/issues/all", methods=["GET"])
@cross_origin()
def issues_api_all():

    results = executeSQL(issuesSQLAll)
    return jsonify(results)


# get issue by name
@application.route("/v1/issues", methods=["GET"])
@cross_origin()
def issues_api_id():

    if "name" in request.args:
        return issues_api_search(request.args["name"])

    return issues_api_all()


# get issues, filtered by given parameters
@application.route("/v1/issues/search", methods=["GET"])
@cross_origin()
def issues_api_search(n="%"):

    results = executeSQL(
        issuesSQL, makeVars(request.args.to_dict(), issuesParams, {"name": n})
    )
    return jsonify(results)


# get all bills
@application.route("/v1/bills/all", methods=["GET"])
@cross_origin()
def bills_api_all():

    results = executeSQL(billsSQLAll)
    makePartyData(results)
    return jsonify(results)


# get bill by bill number
@application.route("/v1/bills", methods=["GET"])
@cross_origin()
def bills_api_id():

    if "name" in request.args:
        return bills_api_search(request.args["name"])

    return bills_api_all()


# get bills, filtered by given parameters
@application.route("/v1/bills/search", methods=["GET"])
@cross_origin()
def bills_api_search(n="%"):

    results = executeSQL(
        billsSQL, makeVars(request.args.to_dict(), billsParams, {"num": n}, "num")
    )
    makePartyData(results)
    return jsonify(results)


# get map data for the api
@application.route("/v1/visualization/map", methods=["GET"])
@cross_origin()
def map_api_visualization():
    return jsonify(mapData)


if __name__ == "__main__":
    application.run()
