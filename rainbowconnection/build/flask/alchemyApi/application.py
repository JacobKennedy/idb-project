from flask import Flask, jsonify, request
import json
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
application = Flask(__name__)

#application.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://{master username}:{db password}@{endpoint}/{db instance name}'
#application.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://rainbow:connection@testgre.cjqoruxzpifj.us-east-2.rds.amazonaws.com/testname'
#application.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://rainbowconnection:SWEisdeath@rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com/congress'
#uri = 'postgresql://rainbow:connection@testgre.cjqoruxzpifj.us-east-2.rds.amazonaws.com/testname'
uri = 'postgresql://rainbowconnection:SWEisdeath@rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com/congress'
application.config['SQLALCHEMY_DATABASE_URI'] = uri
db = SQLAlchemy(application)
engine = db.create_engine(uri)
conn = engine.connect()
metadata = db.MetaData()
metadata.reflect(bind=engine)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    def __repr__(self):
        return '<User %r>' % self.username

@application.route('/', methods=['GET'])
@cross_origin()
def homePage():
    return '<h1>Rainbow Connection API</h1><p>This is a prototype API for getting information about gay stuff.</p>'

@application.route('/v1/congress/all', methods=['GET'])
@cross_origin()
def congress_api_all():
    table = metadata.tables['congress']
    select = db.select([table]).where(True)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/v1/congress', methods=['GET'])
@cross_origin()
def congress_api_id(name):
    name = ''
    check = False
    if 'name' in request.args:
        name = request.args['name']
    else:
        check = True
    table = metadata.tables['congress']
    select = db.select([table]).where(table.c.name == name or check)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]



    return json.dumps(resultset)

issues = [
    {'name': 'Housing',
     'fedProtect': 'No',
     'date': 'N/A',
     'numStates': '22',
     'Description': 'The Office of Fair Housing and Equal Opportunity (FHEO) is an agency within the United States Department of Housing and Urban Development. FHEO is responsible for administering and enforcing federal fair housing laws and establishing policies that make sure all Americans have equal access to the housing of their choice. Housing discrimination refers to discrimination against potential or current tenants by landlords. In the United States, there is no federal law against such discrimination on the basis of sexual orientation or gender identity, but at least twenty-two states and many major cities have enacted laws prohibiting it. See, for example, Washington House Bill 2661.'},
    {'name': 'Medical Facilities',
     'fedProtect': 'Yes',
     'date': '4/14/2010',
     'numStates': '50',
     'desc': 'On April 14, 2010, President Barack Obama issued an Executive Order to the Department of Health and Human Services to draft new rules for all hospitals accepting Medicare or Medicaid funds. They would require facilities to grant visitation and medical decision-making rights to gay and lesbian partners, as well as designees of others such as widows and widowers. Such rights are not protected by law in many states. Obama said he was inspired by the case of a Florida family, where one of the mothers died while her partner and four children were denied visitation by the hospital.'},
    {'name': 'Employment',
     'fedProtect': 'No',
     'date':'N/A',
     'numStates': '22',
     'desc': 'There is no federal statute addressing employment discrimination based on sexual orientation or gender identity. Protections at the national level are limited. Some regulations protect government employees but do not extend their protections to the private sector. Twenty-two states, the District of Columbia, Puerto Rico, and over 140 cities and counties have enacted bans on discrimination based on sexual orientation and/or sexual identity. Employment discrimination refers to discriminatory employment practices such as bias in hiring, promotion, job assignment, termination, and compensation, and various types of harassment. In the United States there is "very little statutory, common law, and case law establishing employment discrimination based upon sexual orientation as a legal wrong.'}
]

@application.route('/issues', methods=['GET'])
@cross_origin()
def issues_api_all():
    table = metadata.tables['issues']
    select = db.select([table]).where(True)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/issues/<name>', methods=['GET'])
@cross_origin()
def issues_api_id(name):
    table = metadata.tables['issues']
    select = db.select([table]).where(table.c.name == name)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

#@application.route('/issues', methods=['GET'])
#@cross_origin()
#def issues_api_all():
#    return json.dumps(issues)
#
#@application.route('/issues/<name>', methods=['GET'])
#@cross_origin()
#def issues_api_id(name):
#    results = []
#    for issue in issues:
#        if issue['name'] == name:
#            results.append(issue)
#    return json.dumps(results)

@application.route('/bills', methods=['GET'])
@cross_origin()
def bills_api_all():
    table = metadata.tables['bills']
    select = db.select([table]).where(True)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/bills/<name>', methods=['GET'])
@cross_origin()
def bills_api_id(name):
    table = metadata.tables['bills']
    select = db.select([table]).where(table.c.name == name)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/printdb', methods=['GET'])
@cross_origin()
def printdb():
    s = 'Tables:<br>'
    for _t in metadata.tables:
        s += ' ' + _t + '<br>...Keys:'
        table = metadata.tables[_t]
        keys = table.columns.keys()
        for k in keys:
            s += ' ' + str(k)
        s += '<br>'
    s += '<br>'
    for _t in metadata.tables:
        table = metadata.tables[_t]
#        keys = table.columns.keys()
#        for k in keys:
#            s += ' ' + str(k)
        select = db.select([table]).where(True)
        res = conn.execute(select)
        resultset = [dict(row) for row in res]
#        return jsonify(resultset)
#        return json.dumps(resultset)
        s += str(resultset) + '<br><br><br><br><br><br><br><br><br>'
    return s

#@application.route('/test', methods=['GET'])
#def testPage():
#    try:
##        db.drop_all(bind=None)
##        db.create_all()
##        admin = User(username='admin', email='admin@example.com')
##        guest = User(username='guest', email='guest@example.com')
##        db.session.add(admin)
##        db.session.add(guest)
##        db.session.commit()
##        x = User.query.filter_by(username='admin').first()
##        s = x.username
##        return s
#        return 'OH SHNIZ CHECK THE DATABASE!'
#    except AttributeError:
#        return 'Oh no. Database can\'t shwift!'
#    else:
#        return 'We Good'
#    return 'Something went wrong.'

@application.route('/hello/<name>')
@cross_origin()
def test(name):
    s = 'Hello ' + name + '!'
    return s

@application.route('/secret', methods=['GET'])
@cross_origin()
def secret():
    return 'oh you found me'

if __name__ == '__main__':
    application.run(host='0.0.0.0')
