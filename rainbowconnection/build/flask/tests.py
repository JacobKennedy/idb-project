from unittest import main, TestCase
from application import *

class MyUnitTests (TestCase) :

    def setUp(self):
        application.config['TESTING'] = True
        application.config['WTF_CSRF_ENABLED'] = False
        application.config['DEBUG'] = False
        self.application = application.test_client()

    def test_home (self) :
        response = self.application.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_congress (self) :
        response = self.application.get('/congress', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_congress_name (self) :
        response = self.application.get('/congress/Ted Cruz', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_congress_connections (self) :
        response = self.application.get('/connections/congress', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_issues (self) :
        response = self.application.get('/issues', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_issues_name (self) :
        response = self.application.get('/issues/Housing', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_issues_connections (self) :
        response = self.application.get('/connections/issues', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_bills (self) :
        response = self.application.get('/bills', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_bills_name (self) :
        response = self.application.get('/bills/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_issues_connections (self) :
        response = self.application.get('/connections/bills', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

if __name__ == "__main__" : # pragma: no cover
    main()

