from flask import Flask, jsonify, request
import json
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
application = Flask(__name__)

#application.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://{master username}:{db password}@{endpoint}/{db instance name}'
#application.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://rainbow:connection@testgre.cjqoruxzpifj.us-east-2.rds.amazonaws.com/testname'
#application.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://rainbowconnection:SWEisdeath@rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com/congress'
#uri = 'postgresql://rainbow:connection@testgre.cjqoruxzpifj.us-east-2.rds.amazonaws.com/testname'
uri = 'postgresql://rainbowconnection:SWEisdeath@rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com/congress'
application.config['SQLALCHEMY_DATABASE_URI'] = uri
db = SQLAlchemy(application)
engine = db.create_engine(uri)
conn = engine.connect()
metadata = db.MetaData()
metadata.reflect(bind=engine)

#name: str, bills: [str], supporters: [str]
iConnections = []
#number: str, cosponsors: [str], issues: [str]
bConnections = []
#name: str, issues: [str], bills: [str]
cConnections = []

def getConn(val, key, connections):
    for c in connections:
        if c[key] == val:
            return c
    #name not found, make a new one
    conn = {key : val}
    connections.append(conn)
    return conn

table = metadata.tables['bills']
select = db.select([table]).where(True)
res = conn.execute(select)
bills = [dict(row) for row in res]
for b in bills:
    #add cosponsors to bill
    bConn = {}
    bConn['number'] = b['number']
    bConn['cosponsors'] = b['cosponsors']
    bConnections.append(bConn)
    #add bill to cosponsors
    for co in b['cosponsors']:
        cConn = getConn(co, 'name', cConnections)
        try:
            cConn['bills'].append(b['number'])
        except KeyError:
            cConn['bills'] = []
            cConn['bills'].append(b['number'])

table = metadata.tables['issues']
select = db.select([table]).where(True)
res = conn.execute(select)
issues = [dict(row) for row in res]
for i in issues:
    #add bills to issue
    iConn = {}
    iConn['name'] = i['name']
    iConn['bills'] = i['connections']
    #add issue to bills
    for co in i['connections']:
        #bills in issues.connections that don't show up in /bills, might be typos
        #TODO: definitely typos, fix them
        #TODO: have better error checking
#        if co != 'H.R.6030' and co != 'H.R.5324' and co != 'H.R.2458':
        bConn = getConn(co, 'number', bConnections)
        try:
            bConn['issues'].append(i['name'])
        except KeyError:
            bConn['issues'] = []
            bConn['issues'].append(i['name'])
    iConnections.append(iConn)

for i in iConnections:
    i['cosponsors'] = []
    #add bill sponsors to issue
    for b in bConnections:
        for c in b['cosponsors']:
            if not (c in i['cosponsors']):
                i['cosponsors'].append(c)
                #add issue to the cosponsor
                cConn = getConn(c, 'name', cConnections)
                try:
                    cConn['issues'].append(i['name'])
                except:
                    cConn['issues'] = []
                    cConn['issues'].append(i['name'])

#table = metadata.tables['issues']
#select = db.select([table]).where(True)
#res = conn.execute(select)
#issues = [dict(row) for row in res]
#for i in issues:
#    #add bills to issue
#    iConn = {}
#    iConn['name'] = i['name']
#    iConn['bills'] = i['connections']
#    #for every bill
#    for co in i['connections']:
#        #add issue to bills
#        bConn = getConn(co, 'number', bConnections)
#        try:
#            bConn['issues'].append(i['name'])
#        except KeyError:
#            bConn['issues'] = []
#            bConn['issues'].append(i['name'])
#        #add the bill's cosponsors to issue
#        for spon in bConn['cosponsors']:
#            try:
#                if not (spon in iConn['cosponsors']):
#                    iConn['cosponsors'].append(spon)
#                    #add the issue to the bill's cosponsor
#                    cConn = getConn(spon, 'name', cConnections)
#                    try:
#                        cConn['issues'].append(i['name'])
#                    except KeyError:
#                        cConn['issues'] = []
#                        cConn['issues'].append(i['name'])
#            except KeyError:
#                iConn['cosponsors'] = []
#                if not (spon in iConn['cosponsors']):
#                    iConn['cosponsors'].append(spon)
#                    #add the issue to the bill's cosponsor
#                    cConn = getConn(spon, 'name', cConnections)
#                    try:
#                        cConn['issues'].append(i['name'])
#                    except KeyError:
#                        cConn['issues'] = []
#                        cConn['issues'].append(i['name'])
#    iConnections.append(iConn)

#@application.route('/testconn', methods=['GET'])
#@cross_origin()
#def testconn():
#	for i in iConnections:
#	    i['cosponsors'] = []
#	    #add bill sponsors to issue
#	    for b in bConnections:
#	        for c in b['cosponsors']:
#	            if not (c in i['cosponsors']):
#	                i['cosponsors'].append(c)
#	                #add issue to the cosponsor
#	                cConn = getConn(c, 'name', cConnections)
#	                try:
#	                    cConn['issues'].append(i['name'])
#	                except:
#	                    cConn['issues'] = []
#	                    cConn['issues'].append(i['name'])
#	return json.dumps(iConnections)

#@application.route('/testconn', methods=['GET'])
#@cross_origin()
#def testconn():
#	s = ' '
#	i = 1
#	for b in bills:
#	    #add cosponsors to bill
#	    bConn = {}
#	    bConn['number'] = b['number']
#	    bConn['cosponsors'] = b['cosponsors']
#	    s += str(i) + str(b['cosponsors']) + '<br>'
#	    i += 1
#	    bConnections.append(bConn)
#	    #add bill to cosponsors
#	    for co in b['cosponsors']:
#	        cConn = getConn(co, 'name', cConnections)
#	        try:
#	            cConn['bills'].append(b['number'])
#	        except KeyError:
#	            cConn['bills'] = []
#	            cConn['bills'].append(b['number'])
#	return s

#TODO: fix putting issues into congress
#TODO: fix putting default cosponsors and issues into bills that don't have any
#TODO: have by name api for connections
#TODO: make sure no duplicate adds

#class User(db.Model):
#    id = db.Column(db.Integer, primary_key=True)
#    username = db.Column(db.String(80), unique=True, nullable=False)
#    email = db.Column(db.String(120), unique=True, nullable=False)
#    def __repr__(self):
#        return '<User %r>' % self.username

@application.route('/', methods=['GET'])
@cross_origin()
def homePage():
    return '<h1>Rainbow Connection API</h1><p>This is a prototype API for getting information about gay stuff.</p>'

@application.route('/congress', methods=['GET'])
@cross_origin()
def congress_api_all():
    table = metadata.tables['congress']
    select = db.select([table]).where(True)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/congress/<name>', methods=['GET'])
@cross_origin()
def congress_api_id(name):
    table = metadata.tables['congress']
    select = db.select([table]).where(table.c.name == name)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/connections/congress', methods=['GET'])
@cross_origin()
def congress_api_connections():
    return json.dumps(cConnections)

#issues = [
#    {'name': 'Housing',
#     'fedProtect': 'No',
#     'date': 'N/A',
#     'numStates': '22',
#     'Description': 'The Office of Fair Housing and Equal Opportunity (FHEO) is an agency within the United States Department of Housing and Urban Development. FHEO is responsible for administering and enforcing federal fair housing laws and establishing policies that make sure all Americans have equal access to the housing of their choice. Housing discrimination refers to discrimination against potential or current tenants by landlords. In the United States, there is no federal law against such discrimination on the basis of sexual orientation or gender identity, but at least twenty-two states and many major cities have enacted laws prohibiting it. See, for example, Washington House Bill 2661.'},
#    {'name': 'Medical Facilities',
#     'fedProtect': 'Yes',
#     'date': '4/14/2010',
#     'numStates': '50',
#     'desc': 'On April 14, 2010, President Barack Obama issued an Executive Order to the Department of Health and Human Services to draft new rules for all hospitals accepting Medicare or Medicaid funds. They would require facilities to grant visitation and medical decision-making rights to gay and lesbian partners, as well as designees of others such as widows and widowers. Such rights are not protected by law in many states. Obama said he was inspired by the case of a Florida family, where one of the mothers died while her partner and four children were denied visitation by the hospital.'},
#    {'name': 'Employment',
#     'fedProtect': 'No',
#     'date':'N/A',
#     'numStates': '22',
#     'desc': 'There is no federal statute addressing employment discrimination based on sexual orientation or gender identity. Protections at the national level are limited. Some regulations protect government employees but do not extend their protections to the private sector. Twenty-two states, the District of Columbia, Puerto Rico, and over 140 cities and counties have enacted bans on discrimination based on sexual orientation and/or sexual identity. Employment discrimination refers to discriminatory employment practices such as bias in hiring, promotion, job assignment, termination, and compensation, and various types of harassment. In the United States there is "very little statutory, common law, and case law establishing employment discrimination based upon sexual orientation as a legal wrong.'}
#]

@application.route('/issues', methods=['GET'])
@cross_origin()
def issues_api_all():
    table = metadata.tables['issues']
    select = db.select([table]).where(True)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/issues/<name>', methods=['GET'])
@cross_origin()
def issues_api_id(name):
    table = metadata.tables['issues']
    select = db.select([table]).where(table.c.name == name)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/connections/issues', methods=['GET'])
@cross_origin()
def issues_api_connections():
    return json.dumps(iConnections)

#@application.route('/issues', methods=['GET'])
#@cross_origin()
#def issues_api_all():
#    return json.dumps(issues)
#
#@application.route('/issues/<name>', methods=['GET'])
#@cross_origin()
#def issues_api_id(name):
#    results = []
#    for issue in issues:
#        if issue['name'] == name:
#            results.append(issue)
#    return json.dumps(results)

@application.route('/bills', methods=['GET'])
@cross_origin()
def bills_api_all():
    table = metadata.tables['bills']
    select = db.select([table]).where(True)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/bills/<number>', methods=['GET'])
@cross_origin()
def bills_api_id(name):
    table = metadata.tables['bills']
    select = db.select([table]).where(table.c.number == number)
    res = conn.execute(select)
    resultset = [dict(row) for row in res]
    return json.dumps(resultset)

@application.route('/connections/bills', methods=['GET'])
@cross_origin()
def bills_api_connections():
    return json.dumps(bConnections)

@application.route('/printdb', methods=['GET'])
@cross_origin()
def printdb():
    s = 'Tables:<br>'
    for _t in metadata.tables:
        s += ' ' + _t + '<br>...Keys:'
        table = metadata.tables[_t]
        keys = table.columns.keys()
        for k in keys:
            s += ' ' + str(k)
        s += '<br>'
    s += '<br>'
    for _t in metadata.tables:
        table = metadata.tables[_t]
#        keys = table.columns.keys()
#        for k in keys:
#            s += ' ' + str(k)
        select = db.select([table]).where(True)
        res = conn.execute(select)
        resultset = [dict(row) for row in res]
#        return jsonify(resultset)
#        return json.dumps(resultset)
        s += str(resultset) + '<br><br><br><br><br><br><br><br><br>'
    return s

#@application.route('/test', methods=['GET'])
#def testPage():
#    try:
##        db.drop_all(bind=None)
##        db.create_all()
##        admin = User(username='admin', email='admin@example.com')
##        guest = User(username='guest', email='guest@example.com')
##        db.session.add(admin)
##        db.session.add(guest)
##        db.session.commit()
##        x = User.query.filter_by(username='admin').first()
##        s = x.username
##        return s
#        return 'OH SHNIZ CHECK THE DATABASE!'
#    except AttributeError:
#        return 'Oh no. Database can\'t shwift!'
#    else:
#        return 'We Good'
#    return 'Something went wrong.'

@application.route('/hello/<name>')
@cross_origin()
def test(name):
    s = 'Hello ' + name + '!'
    return s

@application.route('/secret', methods=['GET'])
@cross_origin()
def secret():
    return 'oh you found me'

if __name__ == '__main__':
    application.run()
