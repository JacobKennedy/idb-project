Flask==1.0.2
Flask-Cors==3.0.7
Flask-SQLAlchemy==2.3.2
Flask-WTF==0.14.2
psycopg2==2.7.7
psycopg2-binary==2.7.7
