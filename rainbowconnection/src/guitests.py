import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome("../frontend/chromedriver")
        self.addCleanup(self.browser.quit)
    
    def testURL1(self):
        self.browser.get('https://www.rainbowconnection.me')
        self.assertIn('Rainbow Connection', self.browser.title)

    def testURL2(self):
        self.browser.get('https://rainbowconnection.me')
        self.assertIn('Rainbow Connection', self.browser.title)

    def testNav1(self):
        self.browser.get('https://rainbowconnection.me')
        self.assertIn('Rainbow Connection', self.browser.title)
        elements = self.browser.find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")
        link = elements[0].find_element_by_class_name("nav-link")
        link.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/")

    def testNav2(self):
        self.browser.get('https://rainbowconnection.me')
        self.assertIn('Rainbow Connection', self.browser.title)
        elements = self.browser.find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")
        link = elements[1].find_element_by_class_name("nav-link")
        link.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/issues")

    def testNav3(self):
        self.browser.get('https://rainbowconnection.me')
        self.assertIn('Rainbow Connection', self.browser.title)
        elements = self.browser.find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")
        link = elements[2].find_element_by_class_name("nav-link")
        link.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/bills")

    def testNav4(self):
        self.browser.get('https://rainbowconnection.me')
        self.assertIn('Rainbow Connection', self.browser.title)
        elements = self.browser.find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")
        link = elements[3].find_element_by_class_name("nav-link")
        link.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/congressMembers")


    def testNav5(self):
        self.browser.get('https://rainbowconnection.me')
        self.assertIn('Rainbow Connection', self.browser.title)
        elements = self.browser.find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")
        link = elements[4].find_element_by_class_name("nav-link")
        link.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/about")
    
    def testNav6(self):
        self.browser.get('https://rainbowconnection.me/issues')
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/issues/Public%20Accomodations")

    def testNav7(self):
        self.browser.get('https://rainbowconnection.me/bills')
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/bills/S.411")

    def testNav8(self):
        self.browser.get('https://rainbowconnection.me/congressMembers')
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://rainbowconnection.me/congressMembers/David%20Young")
    
    #checks issues protected filter
    def testFilter1(self):
        self.browser.get('https://www.rainbowconnection.me/issues')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/issues/Medical%20Facilities")

    #checks issues unprotected filter
    def testFilter2(self):
        self.browser.get('https://www.rainbowconnection.me/issues')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[1]
        selector.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/issues/Conversion%20Therapy")
    
    #checks bills chamber filter
    def testFilter3(self):
        self.browser.get('https://www.rainbowconnection.me/bills')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/bills/H.R.1447")

    #checks bills sponsor filter
    def testFilter4(self):
        self.browser.get('https://www.rainbowconnection.me/bills')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[3].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[3].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/bills/H.R.6033")

    #checks congress state filter
    def testFilter5(self):
        self.browser.get('https://www.rainbowconnection.me/congressMembers')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[2].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/congressMembers/Bradley%20Byrne")
    
    #check congress party filter
    def testFilter6(self):
        self.browser.get('https://www.rainbowconnection.me/congressMembers')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[3].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[3].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(15)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/congressMembers/Adam%20Schiff")
    
    #check issue sort
    def testSort1(self):
        self.browser.get('https://www.rainbowconnection.me/issues')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[1].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[1].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/issues/Conversion%20Therapy")
    
    #checks bills sort
    def testSort2(self):
        self.browser.get('https://www.rainbowconnection.me/bills')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[1].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[1].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(15)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/bills/S.250")
    
    #checks congress sort 
    def testSort3(self):
        self.browser.get('https://www.rainbowconnection.me/congressMembers')
        self.browser.implicitly_wait(5)
        filter = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[1].find_element_by_class_name("dropdown").find_elements_by_id("dropdown-item-button")[0]
        filter.click()
        selector = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[1].find_element_by_class_name("dropdown-menu").find_elements_by_class_name("dropdown-item")[0]
        selector.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/congressMembers/Adam%20Kinzinger")
    
    #checks issue search
    def testSearch1(self):
        self.browser.get('https://www.rainbowconnection.me/issues')
        self.browser.implicitly_wait(5)
        form = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[0].find_element_by_class_name("md-form").find_element_by_class_name("form-control")
        form.send_keys('housing')
        form.submit()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/issues/Housing")
    
    #checks bills search
    def testSearch2(self):
        self.browser.get('https://www.rainbowconnection.me/bills')
        self.browser.implicitly_wait(5)
        form = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[0].find_element_by_class_name("md-form").find_element_by_class_name("form-control")
        form.send_keys('religion')
        form.submit()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/bills/H.R.1447")
    
    #checks congress search
    def testSearch3(self):
        self.browser.get('https://www.rainbowconnection.me/congressMembers')
        self.browser.implicitly_wait(5)
        form = self.browser.find_elements_by_id("navbarResponsive")[1].find_element_by_class_name("navbar-nav").find_elements_by_class_name("nav-item")[0].find_element_by_class_name("md-form").find_element_by_class_name("form-control")
        form.send_keys('Tammy')
        form.submit()
        self.browser.implicitly_wait(15)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/congressMembers/Tammy%20Baldwin")
    
    #checks full site search issue
    def testSearch4(self):
        self.browser.get('https://www.rainbowconnection.me/')
        self.browser.implicitly_wait(5)
        form = self.browser.find_element_by_id("mainNav").find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("md-form").find_element_by_class_name("form-control")
        form.send_keys('housing')
        form.submit()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/issues/Housing")
    
    #checks full site search bill
    def testSearch5(self):
        self.browser.get('https://www.rainbowconnection.me/')
        self.browser.implicitly_wait(5)
        form = self.browser.find_element_by_id("mainNav").find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("md-form").find_element_by_class_name("form-control")
        form.send_keys('religion')
        form.submit()
        self.browser.implicitly_wait(5)
        tab = self.browser.find_element_by_class_name("tab").find_elements_by_class_name("nav-item")[1]
        tab.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("tab-content").find_elements_by_class_name("fade")[1].find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/bills/H.R.1447")
   
    #checks full site search congress
    def testSearch6(self):
        self.browser.get('https://www.rainbowconnection.me/')
        self.browser.implicitly_wait(5)
        form = self.browser.find_element_by_id("mainNav").find_element_by_class_name("container").find_element_by_id("navbarResponsive").find_element_by_class_name("md-form").find_element_by_class_name("form-control")
        form.send_keys('Tammy')
        form.submit()
        self.browser.implicitly_wait(5)
        tab = self.browser.find_element_by_class_name("tab").find_elements_by_class_name("nav-item")[2]
        tab.click()
        self.browser.implicitly_wait(5)
        element = self.browser.find_element_by_class_name("tab-content").find_elements_by_class_name("fade")[2].find_element_by_class_name("modelRow").find_elements_by_class_name("modelColumn")[0].find_element_by_class_name("modelThumbnail")
        element.click()
        self.assertEqual(self.browser.current_url, "https://www.rainbowconnection.me/congressMembers/Tammy%20Baldwin")
    

if __name__ == '__main__':
    unittest.main(verbosity=2)
