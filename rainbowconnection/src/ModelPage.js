import React, { Component } from 'react';
import './App.css';
import "./css/business-casual.css";
import Pagination from "react-js-pagination";
import axios from 'axios';
import { formatCommittees, congressCard } from "./CongressMembers";
import { billCard } from "./Bills";
import { issueCard } from "./Issues";
import { searchCard } from "./Search";
import ModelPageButtons, { getStateAbbreviation } from "./ModelPageButtons";

const ISSUESURL = "https://api.rainbowconnection.me/v1/issues";
const BILLSURL = "https://api.rainbowconnection.me/v1/bills";
const CONGRESSURL = "https://api.rainbowconnection.me/v1/congress";

//the search bar was implemented using the Forms react tutorial: https://reactjs.org/docs/forms.html

//Cuts off desc at cutoff characters and adds ... to the end if desc is greater than maxSize
export function shortenDesc (desc, maxSize = 0, cutoff = 100)
{
    if (desc.length > maxSize)
    {
      var spaceNum = desc.substring(cutoff).indexOf(" ");
      return desc.substring(0, cutoff + spaceNum) + "..."
    }
    return desc;
}

//resizes title text based on length
//argument jsx is a preformated jsx string with the title already in it (used for highlighting)
export function title (t, jsx = undefined)
{
    //use jsx title as the default, use t if there isn't a jsx title
    var title = jsx;
    if (title === undefined)
    {
      title = t;
    }

    //return big text for short titles
    if (t.length < 22)
    {
        return (
          <h3><b>{title}</b></h3>
        );
    }

    //return small text for long titles
    return (
      <h6><b>{title}</b></h6>
    );
}

//shortens on both sides of a search term
export function shortenAround (term, search, maxSize)
{
  if (term.length > maxSize)
  {
    var searchLoc = term.toLowerCase().indexOf(search);

    //shorten end
    var endLoc = searchLoc + maxSize
    if (endLoc <  term.length)
    {
      var spaceEnd = term.substring(endLoc).indexOf(" ");
      term = term.substring(0, endLoc + spaceEnd) + "..."
    }

    //shorten beginning
    if (searchLoc > maxSize)
    {
      var spaceBeg = term.substring(searchLoc - maxSize, term.length).indexOf(" ");
      term = "..." + term.substring(searchLoc - maxSize + spaceBeg + 1, term.length);
    }
  }
  return term;
}

//returns value of attribute with matching search
export function findSearchAttribute (instanceData, search)
{
  search = search.toLowerCase();
  var searchAttributes = [""];
  for (var key in instanceData)
   {
     //alert(key + " " + instanceData[key] + " is a " + typeof(instanceData[key]));
     if (instanceData[key] !== null)
     {
       if (typeof(instanceData[key]) === "string" && instanceData[key].toLowerCase().includes(search) && !instanceData[key].includes("http") && key !== "name")
       {
         //alert("found " + search + " in " + instanceData[key]);
         searchAttributes.push(key + ": " + shortenAround(instanceData[key], search, 50));
       }
       else
       if (typeof(instanceData[key]) !== "string"  &&  key !== "partydata" && key !== "connections")
       {
         //search copsonsors and committees
         var listObjects = formatCommittees(instanceData[key]);
         if (listObjects.toLowerCase().includes(search))
         {
           searchAttributes.push(key + ": " + shortenAround(listObjects, search, 15));
         }
       }
     }
     if (searchAttributes.length > 4)
     {
       break;
     }

   }
   return searchAttributes;
}

export function checkFiltersEmpty (filterValues)
{
  var filtersEmpty = true;
  for (var i = 0; i < filterValues.length; i++)
  {
    if (filterValues[i] !== "")
    {
      filtersEmpty = false;
      break;
    }
  }
  return filtersEmpty;
}

class ModelPage extends Component
{
    constructor(props) {
      super(props);
      this.state =
      {
        activePage: 1,
        cardsPerPage: 9,
        searchValue: "",
        submittedSearchValue: "",
        sortValue: "",
        filterValues: ["", "", ""],
        data: [],
        filterUpdated: true,
        model: this.props.model,
      };


      this.handlePageChange = this.handlePageChange.bind(this);
      this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
      this.handleSortSelect = this.handleSortSelect.bind(this);
      this.handleFilterSelect = this.handleFilterSelect.bind(this);
      this.handleResetButtonPressed = this.handleResetButtonPressed.bind(this);
    }

    //event handler that sets the active page to pageNumber
    handlePageChange(pageNumber)
    {
      this.setState({activePage: pageNumber});
      window.scrollTo(0, 0);
    }

    //event that happens when someone submits a search (presses enter in search form)
    handleSearchSubmit(event)
    {
      this.setState({searchValue: event.target.id, submittedSearchValue: event.target.id, filterUpdated: false});
      event.preventDefault();
    }

    //event that happens when someone presses the reset button
    handleResetButtonPressed(event)
    {
      //alert('Reset button pressed');
      this.setState({sortValue: "", filterValues: ["", "", ""], searchValue: "", submittedSearchValue: "", filterUpdated: false,});
    }

    //event that happens when someone selects a sort dropdown
    handleSortSelect(val)
    {
      this.setState({sortValue: val, filterUpdated: false,});
      //alert('Sort by: ' + val);
    }

    //event that happens when someone selects a filter dropdown
    handleFilterSelect(val, buttonNum = 0)
    {
      var newVal = this.state.filterValues;
      newVal[buttonNum] = val;
      this.setState({filterValues: newVal, filterUpdated: false,});
    }

    getFilterURL (search = "")
    {
      var field = "/search?";

      //use search param for navbar searches that may not update state in time
      if (this.state.submittedSearchValue !== "" && search === "")
      {
        search = this.state.submittedSearchValue;
      }

      if (checkFiltersEmpty(this.state.filterValues) && this.state.sortValue === "" && search === "")
      {
        field = "/all";
      }
      else
      {
        //add search
        if (search !== "" && search !== " ")
        {
          field += "phrase=" + search + "&";
        }

        //add filters
        for (var i = 0; i < this.state.filterValues.length; i++)
        {
          if (this.state.filterValues[i] === "Protected")
          {
            field += "fedProtect=Yes&";
          }
          else if (this.state.filterValues[i] === "Not Protected")
          {
            field += "fedProtect=No&";
          }
          else if (this.state.filterValues[i] === "House")
          {
            field += "num=R.&";
          }
          else if (this.state.filterValues[i] === "Senate")
          {
            field += "num=S.&";
          }
          else if (this.state.filterValues[i] === "Democrat" || this.state.filterValues[i] === "Republican" || this.state.filterValues[i] === "Independent")
          {
            field += "party=" + this.state.filterValues[i] + "&";
          }
          else if (this.state.filterValues[i] === "Republican")
          {
            field += "party=R&";
          }
          else if (this.state.filterValues[i] === "Independent")
          {
            field += "party=I&";
          }
          else if (this.state.filterValues[i] === "2019")
          {
            field += "termend=2019&";
          }
          else if (this.state.filterValues[i] === "2021")
          {
            field += "termend=2021&";
          }
          else if (this.state.filterValues[i] === "2023")
          {
            field += "termend=2023&";
          }
          else if (this.props.model === "bills" && this.state.filterValues[i] !== "")
          {
            field += "sponsor=" + this.state.filterValues[i] + "&";
          }
          else if (this.props.model === "congressMembers" && this.state.filterValues[i] !== "")
          {
            field += "state=" + getStateAbbreviation(this.state.filterValues[i]) + "&";
          }
        }

        //add sorts
        if (this.state.sortValue === "Name (A-Z)")
        {
          field += "sort_by=name&";
        }
        else if (this.state.sortValue === "Name (Z-A)")
        {
          field += "sort_by=name&desc=True&";
        }
        else if (this.state.sortValue === "Number of States protected (increasing)")
        {
          field += "sort_by=numStates&";
        }
        else if (this.state.sortValue === "Number of States protected (decreasing)")
        {
          field += "sort_by=numStates&desc=True&";
        }
        if (this.state.sortValue === "Term End (increasing)")
        {
          field += "sort_by=termend&";
        }
        else if (this.state.sortValue === "Term End (decreasing)")
        {
          field += "sort_by=termend&desc=True&";
        }
        if (this.state.sortValue === "Date Introduced (increasing)")
        {
          field += "sort_by=date&";
        }
        else if (this.state.sortValue === "Date Introduced (decreasing)")
        {
          field += "sort_by=date&desc=True&";
        }
      }
      //reset active page so that we always start on page one when starting a new search/filter/sort
      this.setState({filterUpdated: true, activePage: 1});

      return field;
    }


    setIssueData (data)
    {
      let issues = [];

      //now only need to get attributes
      for (let i = 0; i < data.length; i++)
      {
          var n = {};
          n.name = data[i].name;
          if (this.state.searchValue !== "")
          {
            n.search = findSearchAttribute(data[i], this.state.searchValue);
          }


          n.fedProtect = data[i].fedProtect;
          if (n.fedProtect === undefined)
          {
            n.fedProtect = data[i].fedprotect;
          }

          n.date = data[i].date;
          n.numStates = data[i].numStates;
          if (n.numStates === undefined)
          {
            n.numStates = data[i].numstates;
          }

          n.desc = data[i].desc;
          if (n.desc === undefined)
          {
            n.desc = data[i].des;
          }

          n.icon = data[i].icon;
          if (n.icon === undefined)
          {
              n.icon = "https://www.algbtical.org/equality00.jpg";
          }

          issues.push(n);
      }

      this.setState({
        isLoaded: true,
        data : issues,
      });
    }

    setCongressData (data)
    {
      let congress = [];
      for (let i = 0; i < data.length; i++)
      {
          var n = {};
          n.name = data[i].name;
          if (this.state.searchValue !== "")
          {
            n.search = findSearchAttribute(data[i], this.state.searchValue);
          }
          n.title = data[i].title;

          n.party = data[i].party;

          n.phone = data[i].phone;
          n.office = data[i].office;
          n.icon = "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/" + data[i].memberid + ".jpg";
          n.contact = data[i].contact;
          if (n.contact === undefined)
          {
            n.contact = data[i].contact_form;
          }
          n.state = data[i].state;
          n.termEnd = data[i].termend;
          if (n.termEnd === undefined)
          {
            n.termEnd = data[i].term_end;
          }

          congress.push(n);
      }

      this.setState({
        isLoaded: true,
        data : congress,
      });
    }

    setBillData (data)
    {
      let bills = [];

      for (let i = 0; i < data.length; i++)
      {
          var n = {};
          n.name = data[i].name;
          if (this.state.searchValue !== "")
          {
            n.search = findSearchAttribute(data[i], this.state.searchValue);
          }

          n.number = data[i].num;
          if (n.number === undefined)
          {
            n.number = data[i].number;
          }
          n.date = data[i].date;
          if (n.date === undefined)
          {
            n.date = data[i].intro_date;
          }
          n.sponsor = data[i].sponsor;
          n.summary = data[i].summary;
          n.cosponsors = data[i].cosponsors;
          n.link = data[i].link;
          //n.icon = "http://www.investvanuatu.org/wp/wp-content/uploads/2019/01/icon.png";
          n.icon = data[i].connections[1];
          bills.push(n);
      }


      this.setState({
        isLoaded: true,
        data : bills,
      });
    }

    async componentDidUpdate()
    {

      if (!this.state.filterUpdated)
      {
        if (this.props.model === "issues")
        {
          await axios.get(ISSUESURL + this.getFilterURL()).then(
            result => {

              this.setIssueData(result.data);
            },

            error => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          );
        }
        else if (this.props.model === "bills")
        {
          await axios.get(BILLSURL + this.getFilterURL()).then(
            result => {

              this.setBillData(result.data);
            },

            error => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          );
        }
        if (this.props.model === "congressMembers")
        {
          await axios.get(CONGRESSURL + this.getFilterURL()).then(
            result => {

              this.setCongressData(result.data);
            },

            error => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          );
        }
      }
    }

    async componentDidMount()
    {
      var urlTag = "/all";
      var searchFromNav = this.props.searchValue;
      //get search terms from navbar search
      if (searchFromNav !== undefined && this.state.searchValue !== searchFromNav)
      {
        this.setState({searchValue: searchFromNav, submittedSearchValue: searchFromNav, filterUpdated: false});
        urlTag = this.getFilterURL(searchFromNav);
      }

      if (this.props.model === "issues")
      {
        await axios.get(ISSUESURL + urlTag).then(
          result => {

            this.setIssueData(result.data);
          },

          error => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        );
      }
      else if (this.props.model === "bills")
      {
        await axios.get(BILLSURL + urlTag).then(
          result => {

            this.setBillData(result.data);
          },

          error => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        );
      }
      if (this.props.model === "congressMembers")
      {
        await axios.get(CONGRESSURL + urlTag).then(
          result => {

            this.setCongressData(result.data);
          },

          error => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        );
      }
    }

    modelCard (i)
    {
      if (this.state.submittedSearchValue !== "" && this.state.submittedSearchValue !== " ")
      {
        return searchCard(this.state.data[i], this.props.model, this.state.submittedSearchValue);
      }

      if (this.props.model === "issues")
      {
        return issueCard(this.state.data[i]);
      }

      if (this.props.model === "bills")
      {
        return billCard(this.state.data[i]);
      }

      if (this.props.model === "congressMembers")
      {
        return congressCard(this.state.data[i]);
      }
    }

    generateCards ()
    {

      var len = this.state.data.length;
      let cards = [];
      let rowCards = [];

      var start = (this.state.activePage - 1) * this.state.cardsPerPage;
      if (len > (start + this.state.cardsPerPage))
      {
        len =  (start + this.state.cardsPerPage);
      }
      //alert (start);
      for (var i = start; i < len; i++)
      {

        rowCards.push(this.modelCard (i));
        if (i % 3 === 2)
        {
          cards.push(
                <div>
                    <div className="modelRow">
                      {rowCards}
                    </div>
                <br/>
                </div>
          );
          rowCards = [];
        }
      }

      cards.push(
        <div>
        <div className="modelRow">
          {rowCards}
        </div>
        <br/>
        </div>
      );

      return (
        <div>
          <div align="center" style= {{margin:"30px"}}>
            {cards}

          </div>
        </div>
      );
    }

    //formats text after a search to say "Found __ results for "__" in model
    generateSearchText ()
    {
      if (this.state.submittedSearchValue === "")
      {
        return;
      }

      var model = this.props.model;
      if (model === "congressMembers")
      {
        model = "members of congress"
      }

      //say we are returning all results if searchValue is space (user story)
      if (this.state.submittedSearchValue === " ")
      {
        return (
          <div className="pageBox" align="center">
            <br />
            <h2>Showing all results for {model}:</h2>
            <br />
          </div>
        );
      }


      return (
        <div className="pageBox" align="center">
          <br />
          <h2>Found {this.state.data.length} results for "{this.state.submittedSearchValue}" in {model}:</h2>
          <br />
        </div>
      );
    }

    getModelPageButtons ()
    {
      if (this.props.showSearch === undefined)
      {
        return (
          <ModelPageButtons
              model={this.props.model}
              filterValues={this.state.filterValues}
              sortValue={this.state.sortValue}
              filterHandler={this.handleFilterSelect}
              sortHandler={this.handleSortSelect}
              searchSubmitHandler={this.handleSearchSubmit}
              searchValue={this.state.searchValue}
              resetHandler={this.handleResetButtonPressed}
          />
        );
      }

    }

    render()
    {
      //get search terms from navbar search
      if (this.props.searchValue !== undefined && this.state.searchValue !== this.props.searchValue)
      {
        var val = this.props.searchValue;

        this.setState({searchValue: val, submittedSearchValue: val, filterUpdated: false});
      }

      return (
        <div>
          {this.getModelPageButtons()}
          {this.generateSearchText()}
          {this.generateCards()}
          <Pagination
            activePage={this.state.activePage}
            prevPageText='prev'
            nextPageText='next'
            firstPageText='first'
            lastPageText='last'
            itemsCountPerPage={this.state.cardsPerPage}
            totalItemsCount={this.state.data.length}
            pageRangeDisplayed={5}
            onChange={this.handlePageChange}
            itemClass="page-item"
            linkClass="page-link"
          />
          <br/>
        </div>
      );
    }
}

export default ModelPage;
