import React, { Component } from 'react';
import './App.css';
import "./css/business-casual.css";

class ErrorPage extends Component
{
  render()
  {
      return (
        <div>
         <br />

         <div className="pageBox" align="center">
         <br />
         <p>The page you were looking for could not be found.</p>
         <br />
         </div>
        </div>
      );
  }
}

export default ErrorPage;
