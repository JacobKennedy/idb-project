import React, { Component } from 'react';
import {Link, withRouter} from 'react-router-dom';
import "./css/business-casual.css";

class Slideshow extends Component {
  render() {
    return (
        <div className="bd-example">
          <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
              <li data-target="#carouselExampleCaptions" data-slide-to="0" className="active"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            </ol>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img src="./img/Slide1.jpg" className="d-block w-100" alt="..."/>
                <div className="carousel-caption d-none d-md-block">
                </div>
              </div>
              <div className="carousel-item">
                <img src="./img/Slide2.jpg" className="d-block w-100" alt="..."/>
                <div className="carousel-caption d-none d-md-block">
                </div>
              </div>
              <div className="carousel-item">
                <img src= "./img/Slide3.jpg" className="d-block w-100" alt="..."/>
                <div className="carousel-caption d-none d-md-block">
                </div>
              </div>
              <div className="carousel-item">
                <img src="./img/Slide4.jpg" className="d-block w-100" alt="..."/>
                <div className="carousel-caption d-none d-md-block">
                </div>
              </div>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
              <span className="carousel-control-prev-icon" aria-hidden="true"></span>
              <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
              <span className="carousel-control-next-icon" aria-hidden="true"></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
          <div className="pageBox" align="center">
          <br></br>
          <table>
            <tr>
            <td><Link to = "/issues" className="indexThumbnail"><img src="./img/issues.png" /></Link></td>
            <br></br>
            <td><Link to = "/bills" className="indexThumbnail"><img src="./img/bills.png" /></Link></td>
            <br></br>
            <td><Link to = "/congressMembers" className="indexThumbnail"><img src="./img/member.png" /></Link></td>
            </tr>
            <br></br>
             <tr>

            <td className="indexText"><Link to = "/issues"><p><b>Issues</b><br></br> that LGBT+ people face</p></Link></td>
             <br></br>
            <td className="indexText"><Link to = "/issues"><p> <b>Bills</b> concerning LGBT+ issues</p></Link></td>
             <br></br>
            <td className="indexText"><Link to = "/issues"><p><b>Members of Congress</b> that vote on LGBT+ bills</p></Link></td>
            
            </tr>
            <br></br>
          </table>
          </div>
        </div>
    );
  }
}

export default Slideshow;
