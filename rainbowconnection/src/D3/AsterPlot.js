import React, { Component } from 'react';
import * as d3 from "d3";
import d3Tip  from "d3-tip";

import "./css/AsterPlotStyle.css";

//This code is from Aster Plot on D3: http://bl.ocks.org/bbest/2de0e25d4840c68f2db1

const colorData = ["#9E0041", "#C32F4B", "#E1514B", "#F47245", "#FB9F59", "#FEC574", "#FAE38C", "#EAF195", "#C7E89E", "#9CD6A4", "#6CC4A4", "#4D9DB4", "#4776B4", "#5E4EA1"];
var currentColorNum = 1;

export function getNextColor ()
{
  currentColorNum += 2;
  if (currentColorNum >= colorData.length)
  {
    currentColorNum = 0;
  }
  return colorData[currentColorNum];
}

class AsterPlot extends Component
{
  drawAsterPlot ()
  {

    var width = 500,
    height = 500,
    radius = Math.min(width, height) / 2,
    innerRadius = 0.3 * radius;

    var pie = d3.layout.pie()
        .sort(null)
        .value(function(d) { return d.width; });

    const tip = d3Tip()
      .attr('class', 'd3-tip')
      .offset([0, 0])
      .html(function(d) {
        return d.data.label + ": <span style='color:orangered'>" + d.data.score + "</span>";
      });

    var arc = d3.svg.arc()
      .innerRadius(innerRadius)
      .outerRadius(function (d) {
        return (radius - innerRadius) * (d.data.score / 50.0) + innerRadius;
      });

    var outlineArc = d3.svg.arc()
            .innerRadius(innerRadius)
            .outerRadius(radius);

    var svg = d3.select("plot").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    svg.call(tip);

    d3.json(this.props.dataURL, function(error, data) {

      data.forEach(function(d) {
        d.color  =  getNextColor();
        d.score  = +d.numstates;
        d.width  = 1;
        d.label = d.name;
      });
       //for (var i = 0; i < data.score; i++) { alert(data[i].id) }

      //inner circle
      svg.selectAll(".solidArc")
          .data(pie(data))
        .enter().append("path")
          .attr("fill", function(d) { return d.data.color; })
          .attr("class", "solidArc")
          .attr("stroke", "gray")
          .attr("d", arc)
          .on('mouseover', tip.show)
          .on('mouseout', tip.hide);

      //outer circle
      svg.selectAll(".outlineArc")
          .data(pie(data))
        .enter().append("path")
          .attr("fill", "none")
          .attr("stroke", "gray")
          .attr("class", "outlineArc")
          .attr("d", outlineArc);

    });
  }

  componentDidMount() {
    this.drawAsterPlot();
  }

  render()
  {
    return (
      <div>

      <plot/>
        <br/>
      </div>
    );
  }
}

export default AsterPlot;
