
import React from 'react';
import App, { findInstance } from './App';
import { Route, Link, BrowserRouter } from 'react-router-dom';
import { shortenDesc, title, shortenAround} from './ModelPage';
import { getStateAbbreviation } from './ModelPageButtons';
import { formatCommittees, getBoolfromString, createSocialMediaLink } from "./CongressMembers";
import { issueMap } from "./Issues";
import { linkCosponsors, linkSponsor, billDescStart } from "./Bills";
import { formatIconName, formatTool } from "./About";
import { highlightText } from "./Search";
import Connections from "./Connections.js";
import ErrorPage from "./ErrorPage.js";
import { mountWrap } from "../test/contextWrapper";

describe('Local State', () => {

  //modelPage tests
  //shortenDesc
  it('should add ... to desc shorter than 100 characters', () => {
    expect(shortenDesc("hi")).to.equal("hi...");
  });

  it('should shorten the name of the bill and add ...', () => {
    expect(shortenDesc("A bill to impose sanctions with respect to foreign persons responsible for gross violations of internationally " +
    "recognized human rights against lesbian, gay, bisexual, and transgender (LGBT) individuals, and for other purposes.")).to.equal(
    "A bill to impose sanctions with respect to foreign persons responsible for gross violations of internationally...");
  });

  it('should shorten the description of an issue and add ...', () => {
    expect(shortenDesc("The Office of Fair Housing and Equal Opportunity (FHEO) is an agency within the United States Department of " +
    "Housing and Urban Development. FHEO is responsible for administering and enforcing federal fair housing laws and establishing " +
    "policies that make sure all Americans have equal access to the housing of their choice. Housing discrimination refers to " +
    "discrimination against potential or current tenants by landlords. In the United States, there is no federal law against such " +
    "discrimination on the basis of sexual orientation or gender identity, but at least twenty-two states and many major cities have " +
    "enacted laws prohibiting it. See, for example, Washington House Bill 2661.")).to.equal(
    "The Office of Fair Housing and Equal Opportunity (FHEO) is an agency within the United States Department...");
  });

  it('should not add ... or shorten title of this congress member', () => {
      expect(shortenDesc("Tammy Baldwin", 50, 50)).to.equal("Tammy Baldwin");
  });

  it('should add ... and shorten title of this medium length bill', () => {
      expect(shortenDesc("A bill to eliminate racial, religious, and other discriminatory profiling by law enforcement, and for other purposes.", 50, 50)).to.equal("A bill to eliminate racial, religious, and other discriminatory...");
  });

  it('should add ... and shorten title of this long bill', () => {
      expect(shortenDesc("A bill to amend the Voting Rights Act of 1965 to revise the criteria for determining which States and political subdivisions are subject to section 4 of the Act, and for other purposes.", 50, 50)).to.equal("A bill to amend the Voting Rights Act of 1965 to revise...");
  });

  //getStateAbbreviation
  it('should get the abbreviation for Texas', () => {
      expect(getStateAbbreviation("Texas")).to.equal("TX");
  });

  it('should get the abbreviation for Minnesota', () => {
      expect(getStateAbbreviation("Minnesota")).to.equal("MN");
  });

  it('should return empty string if not given a state', () => {
      expect(getStateAbbreviation("West Xylophone")).to.equal("");
  });

  //title
  it('should make the title big (h3) if it is short', () => {
      const app = shallow(title("Education"));
      expect(app.containsMatchingElement(<h3><b>Education</b></h3>)).to.equal(true);
  });

  it('should make the title smaller (h6) if it is long', () => {
      var testTitle = "A bill to eliminate racial, religious, and other discriminatory profiling by law enforcement, and for other purposes.";
      var wrapper = shallow(title(testTitle));
      const app = shallow(title(testTitle));
      expect(app.containsMatchingElement(<h6><b>{testTitle}</b></h6>)).to.equal(true);
  });

  //linksponsor
  it('it should link a sponsor', () => {
      const app = mountWrap(linkSponsor("Tammy Baldwin", "Tammy Baldwin"));
      expect(app.containsMatchingElement
        (
          <Link to="/congressMembers/Tammy Baldwin">
            Tammy Baldwin
          </Link>
        )).to.equal(true);
  });

  //shortenAround
  it('should shorten both sides of a long description around search term', () => {
      var desc = "Public accommodations refers to both governmental entities and private businesses that provide services" +
      "to the general public such as restaurants, movie theaters, libraries and shops. It does not encompass private clubs that have a membership or dues process.";
      expect(shortenAround(desc, "libraries", 50)).to.equal("...public such as restaurants, movie theaters, libraries and shops. It does not encompass private...");
  });

  //highlightText
  it('should highlight search term', () => {

      const app = shallow(<div>{highlightText("...Steve Cohen, Bill Keating, Rick...", "bill")}</div>);
      expect(app.containsMatchingElement
        (
          <div>
            ...Steve Cohen, <strong>Bill</strong> Keating, Rick...
         </div>
       )).to.equal(true);
  });

  //congressMembers tests
  //format committees
  it('should format with commas between list elements', () => {
      expect(formatCommittees(["a", "b"],)).to.equal("a, b");
  });

  it('should format with a commas between committees', () => {
      expect(formatCommittees([
      "Committee on Appropriations",
      "Committee on Energy and Natural Resources",
      "Committee on Health, Education, Labor, and Pensions",
      "Committee on Rules and Administration"
  ],)).to.equal("Committee on Appropriations, Committee on Energy and Natural Resources, Committee on Health, Education, Labor, and Pensions, Committee on Rules and Administration");
  });

  //getBoolfromString
  it('should return true when given string True', () => {
      expect(getBoolfromString("True")).to.equal(true);
  });

  it('should return false when given string False', () => {
      expect(getBoolfromString("False")).to.equal(false);
  });

  //createSocialMediaLink
  it('should make a social media link', () => {
      const app = shallow(createSocialMediaLink("SenatorBaldwin", "https://twitter.com/SenatorBaldwin?lang=en"));
      expect(app.containsMatchingElement(<b><a href="https://twitter.com/SenatorBaldwin?lang=en">SenatorBaldwin</a></b>)).to.equal(true);
  });

  it('should display N/A with no link', () => {
      const app = shallow(createSocialMediaLink(null, "https://facebook.com/" + null));
      expect(app.containsMatchingElement(<b>N/A</b>)).to.equal(true);
  });

  //issues tests
  //issueMap
  it('should render map in iframe if it is an embedded map', () => {
      const app = shallow(issueMap("https://www.hrc.org/state-maps/public-accomodations/embed/"));
      expect(app.containsMatchingElement
        (
          <div align="center" style= {{margin:"30px"}}>
             <iframe src="https://www.hrc.org/state-maps/public-accomodations/embed/"
               title = "map"
               width="760"
               height="700"
               frameBorder="0"
               seamless="seamless"
               scrolling="auto">
             </iframe>
         </div>
        )).to.equal(true);
  });

  it('should render map as an image if it is not an embedded map', () => {
      const app = shallow(issueMap("https://gitlab.com/JacobKennedy/idb-project/raw/master/rainbowconnection/public/img/employment.png"));
      expect(app.containsMatchingElement(<img className="issueMap" src="https://gitlab.com/JacobKennedy/idb-project/raw/master/rainbowconnection/public/img/employment.png" alt="map"/>)).to.equal(true);
  });

  //bills tests
  //billDescStart
  it('should start the bill description at \"This bill\"', () => {
      var wholeSummary = "End Racial and Religious Profiling Act of 2017 or ERRPA This bill prohibits racial profiling by a federal, state, local, or tribal law enforcement agency or agent. The term \"racial profiling\" includes the practice of relying on actual or perceived race, ethnicity, national origin, religion, gender, gender identify, or sexual orientation in making a routine or spontaneous law enforcement decision. The Department of Justice (DOJ), or an individual injured by racial profiling, may bring a civil action to enforce the prohibition. The bill requires federal law enforcement agencies to maintain policies and procedures to eliminate racial profiling, including training on racial profiling issues, the collection of data, and procedures for handling complaints. Additionally, a state or unit of local government that receives grant funds under the Edward Byrne Memorial Justice Assistance Grant program or Community Oriented Policing Services program must maintain policies and procedures to eliminate racial profiling, including training on racial profiling issues, the collection of data, and participation in an administrative complaint procedure or independent audit program. DOJ must withhold or reduce grant funds from a state or unit of local government that fails to comply.  The bill authorizes grants to develop and implement: (1) programs to collect data on the percentage of stops and searches in which a law enforcement officer finds drugs, a gun, or something else that leads to an arrest; and (2) best practice devices and systems to eliminate racial profiling. DOJ must report annually on racial profiling by law enforcement agencies."
      var summary = "This bill prohibits racial profiling by a federal, state, local, or tribal law enforcement agency or agent. The term \"racial profiling\" includes the practice of relying on actual or perceived race, ethnicity, national origin, religion, gender, gender identify, or sexual orientation in making a routine or spontaneous law enforcement decision. The Department of Justice (DOJ), or an individual injured by racial profiling, may bring a civil action to enforce the prohibition. The bill requires federal law enforcement agencies to maintain policies and procedures to eliminate racial profiling, including training on racial profiling issues, the collection of data, and procedures for handling complaints. Additionally, a state or unit of local government that receives grant funds under the Edward Byrne Memorial Justice Assistance Grant program or Community Oriented Policing Services program must maintain policies and procedures to eliminate racial profiling, including training on racial profiling issues, the collection of data, and participation in an administrative complaint procedure or independent audit program. DOJ must withhold or reduce grant funds from a state or unit of local government that fails to comply.  The bill authorizes grants to develop and implement: (1) programs to collect data on the percentage of stops and searches in which a law enforcement officer finds drugs, a gun, or something else that leads to an arrest; and (2) best practice devices and systems to eliminate racial profiling. DOJ must report annually on racial profiling by law enforcement agencies."
      expect(billDescStart(wholeSummary)).to.equal(summary);
  });

  //linkCosponsors
  it('should list a single cosponsor with a link', () => {
      var cosponsors = ["Tammy Baldwin",];
      const app = mountWrap(linkCosponsors(cosponsors)[0]);

      expect(app.containsMatchingElement
        (
          <Link to={"/congressMembers/Tammy Baldwin"}>Tammy Baldwin</Link>
        )).to.equal(true);
  });

  it('should list two cosponsors with links and a comma between them', () => {
      var cosponsors = ["Tammy Baldwin", "Richard Blumenthal"];
      var linkedCosponsors = linkCosponsors(cosponsors);
      const app1 = mountWrap(linkedCosponsors[0]);
      const app2 = mountWrap(linkedCosponsors[1]);

      expect(app1.containsMatchingElement
        (
          <Link to={"/congressMembers/Tammy Baldwin"}>Tammy Baldwin, </Link>
        )).to.equal(true);
      expect(app2.containsMatchingElement
        (
          <Link to={"/congressMembers/Richard Blumenthal"}>Richard Blumenthal</Link>
        )).to.equal(true);
  });

  //connections tests
  it('it should add a bill connection', () => {
      var connection = [{ model: "bills",
                          name:  "S.1328",
                          icon:  "http://www.investvanuatu.org/wp/wp-content/uploads/2019/01/icon.png"
                        }]


                      //    { model: "congressMembers",
                        //    name:  "Tim Kaine",
                        //    icon:  "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/K000384.jpg"}];
      const app = mountWrap(<Connections connections = {connection} model = "issue"/>);
      expect(app.containsMatchingElement
        (
          <div>
          <div align="center" style= {{margin: "50px"}}>
            <div className="linkRow">
              <div>
              <div className="linkColumn">
                <div className="modelIcon-link">
                <div className="modelInfo">
                <Link to={"/bills/S.1328"}>
                  <img className= "modelThumbnail" src="http://www.investvanuatu.org/wp/wp-content/uploads/2019/01/icon.png" alt="..."/>
                </Link>
                <h6><b>Related bill:</b></h6>
                <h6>S.1328</h6>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          </div>
        )).to.equal(true);
  });

  it('it should add an issue connection for the bill model', () => {
      var connection = [{ model: "issues",
                          name:  "Employment",
                          icon:  "https://static1.squarespace.com/static/53e51960e4b0f38ca4081a61/t/5ba908da9140b79ed8b8664b/1537804514336/o-ENDA-facebook.jpg?format=750w"
                        }]

      const app = mountWrap(<Connections connections = {connection} model = "bill"/>);
      expect(app.containsMatchingElement
        (
          <div>
          <div align="center" style= {{margin: "50px"}}>
            <div className="linkRow">
              <div>
              <div className="linkColumn">
                <div className="modelIcon-link">
                <div className="modelInfo">
                <Link to={"/issues/Employment"}>
                  <img className= "modelThumbnail" src="https://static1.squarespace.com/static/53e51960e4b0f38ca4081a61/t/5ba908da9140b79ed8b8664b/1537804514336/o-ENDA-facebook.jpg?format=750w" alt="..."/>
                </Link>
                <h6><b>Related issue:</b></h6>
                <h6>Employment</h6>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          </div>
        )).to.equal(true);
  });

  it('it should add an issue connection for a supporting congress member', () => {
      var connection = [{ model: "issues",
                          name:  "Employment",
                          support: true,
                          icon:  "https://static1.squarespace.com/static/53e51960e4b0f38ca4081a61/t/5ba908da9140b79ed8b8664b/1537804514336/o-ENDA-facebook.jpg?format=750w"
                        }]

      const app = mountWrap(<Connections connections = {connection} model = "congress"/>);
      expect(app.containsMatchingElement
        (
          <div>
          <div align="center" style= {{margin: "50px"}}>
            <div className="linkRow">
              <div>
              <div className="linkColumn">
                <div className="modelIcon-link">
                <div className="modelInfo">
                <Link to={"/issues/Employment"}>
                  <img className= "modelThumbnail" src="https://static1.squarespace.com/static/53e51960e4b0f38ca4081a61/t/5ba908da9140b79ed8b8664b/1537804514336/o-ENDA-facebook.jpg?format=750w" alt="..."/>
                </Link>
                <h6><b>This congress member supports this issue:</b></h6>
                <h6>Employment</h6>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          </div>
        )).to.equal(true);
  });

  it('it should add an issue connection for a non-supporting congress member', () => {
      var connection = [{ model: "issues",
                          name:  "Employment",
                          support: false,
                          icon:  "https://static1.squarespace.com/static/53e51960e4b0f38ca4081a61/t/5ba908da9140b79ed8b8664b/1537804514336/o-ENDA-facebook.jpg?format=750w"
                        }]

      const app = mountWrap(<Connections connections = {connection} model = "congress"/>);
      expect(app.containsMatchingElement
        (
          <div>
          <div align="center" style= {{margin: "50px"}}>
            <div className="linkRow">
              <div>
              <div className="linkColumn">
                <div className="modelIcon-link">
                <div className="modelInfo">
                <Link to={"/issues/Employment"}>
                  <img className= "modelThumbnail" src="https://static1.squarespace.com/static/53e51960e4b0f38ca4081a61/t/5ba908da9140b79ed8b8664b/1537804514336/o-ENDA-facebook.jpg?format=750w" alt="..."/>
                </Link>
                <h6><b>This congress member does not support this issue:</b></h6>
                <h6>Employment</h6>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          </div>
        )).to.equal(true);
  });

  it('it should add an congress connection', () => {
      var connection = [{  model: "congressMembers",
                          name:  "Tim Kaine",
                          icon:  "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/K000384.jpg"}];

      const app = mountWrap(<Connections connections = {connection} model = "bill"/>);
      expect(app.containsMatchingElement
        (
          <div>
          <div align="center" style= {{margin: "50px"}}>
            <div className="linkRow">
              <div>
              <div className="linkColumn">
                <div className="modelIcon-link">
                <div className="modelInfo">
                <Link to={"/congressMembers/Tim Kaine"}>
                  <img className= "modelThumbnail" src="https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/K000384.jpg" alt="..."/>
                </Link>
                <h6><b>Member of congress who supports this bill:</b></h6>
                <h6>Tim Kaine</h6>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          </div>
        )).to.equal(true);
  });

  //test ErrorPage
  it('it should print error text on the 404 error page', () => {
      const app = shallow(<ErrorPage/>);
      expect(app.containsMatchingElement
        (
          <div>
           <br />

           <div className="pageBox" align="center">
           <br />
           <p>The page you were looking for could not be found.</p>
           <br />
           </div>
          </div>
        )).to.equal(true);
  });

  //test About page
  //formatIconName
  it('should format icon name with no spaces', () => {
      expect(formatIconName("GitLab", ".jpeg")).to.equal("img/tools/gitlab.jpeg");
  });

  it('should format icon name with spaces', () => {
      expect(formatIconName("Elastic Beanstalk", ".png")).to.equal("img/tools/elasticbeanstalk.png");
  });

  //formatTool
  it('it should format tool with default extension (png)', () => {
      const app = shallow(formatTool("Slack", "Used for for communication"));
      expect(app.containsMatchingElement
        (
          <div className="modelColumn">
           <div className="toolIcon">
                 <img src={"img/tools/slack.png"} className="rounded mx-auto d-block" alt=""/>
               </div>
           <p><b>Slack</b></p>
           <p>Used for for communication</p>
          </div>
        )).to.equal(true);
  });

  it('it should format tool with non-default extension (jpeg)', () => {
      const app = shallow(formatTool("GitLab", "Used for version control", ".jpeg"));
      expect(app.containsMatchingElement
        (
          <div className="modelColumn">
           <div className="toolIcon">
                 <img src={"img/tools/gitlab.jpeg"} className="rounded mx-auto d-block" alt=""/>
               </div>
           <p><b>GitLab</b></p>
           <p>Used for version control</p>
          </div>
        )).to.equal(true);
  });


});
