import React, { Component } from 'react';
import axios from 'axios';

import './App.css';
import "./css/business-casual.css";

const PROJECTID = 10923752;
const ISSUESURL = `https://gitlab.com/api/v4/projects/${PROJECTID}/issues?per_page=1000`;
const COMMITSURL = `https://gitlab.com/api/v4/projects/10923752/repository/commits?all=true&per_page=100`;

//remove spaces, make lowercase, and add extension
export function formatIconName (name, extension)
{
  return "img/tools/" + name.toLowerCase().replace(/\s/g, '') + extension;
}

//generates a tool with icon, name, and description
export function formatTool (name, desc, extension = ".png")
{
  return (
    <div className="modelColumn">
     <div className="toolIcon">
           <img src={formatIconName(name, extension)} className="rounded mx-auto d-block" alt=""/>
         </div>
     <p><b>{name}</b></p>
     <p>{desc}</p>
    </div>
  );
}

class About extends Component
{
  constructor(props)
  {
    super(props);
    this.state =
    {
        error: null,
        isLoaded: false,
        team: {amber:     {commits: 0, issuesOpened: 0, issuesClosed: 0, unitTests: 35},
               elizabeth: {commits: 0, issuesOpened: 0, issuesClosed: 0, unitTests: 14},
               jacob:     {commits: 0, issuesOpened: 0, issuesClosed: 0, unitTests: 40},
               joseph:    {commits: 0, issuesOpened: 0, issuesClosed: 0, unitTests: 12},
               karishma:  {commits: 0, issuesOpened: 0, issuesClosed: 0, unitTests: 15},
               total:     {commits: 0, issuesOpened: 0, issuesClosed: 0, unitTests: 0}},
    };
  }

  //adds up unit tests for every member
  calculateUnitTests ()
  {
    var team = this.state.team;
    team.total.unitTests = team.amber.unitTests + team.elizabeth.unitTests + team.jacob.unitTests + team.joseph.unitTests + team.karishma.unitTests;
    this.setState({
      team : team
    });
  }

  calculateCommits (data)
  {
      let team = this.state.team;

      for (let i = 0; i < data.length; i++)
      {
          let name = data[i].author_name;
          if (name === "as75287" || name === "Amber Smith")
          {
              team.amber.commits++;
          }
          else if (name === "Elizabeth Funk" || name === "elizabeth_funk")
          {
              team.elizabeth.commits++;
          }
          else if (name === "Jacob Kennedy")
          {
              team.jacob.commits++;
          }
          else if (name === "joexia")
          {
              team.joseph.commits++;
          }

          else if (name === "Karishma" || name === "Karishma Patel")
          {
              team.karishma.commits++;
          }

          team.total.commits++;
      }
      return team;
  }

  calculateIssues (data)
  {
      let team = this.state.team;

      for (let i = 0; i < data.length; i++)
      {
          let authorName = data[i].author.name;
      		if (authorName === "Amber Smith")
      		{
      			team.amber.issuesOpened++;
      		}
      		else if (authorName === "Jacob Kennedy")
      		{
      			team.jacob.issuesOpened++;
      		}
      		else if (authorName === "Joseph Xia")
      		{
      			team.joseph.issuesOpened++;
      		}
      		else if (authorName === "Karishma Patel")
      		{
      			team.karishma.issuesOpened++;
      		}
      		else if (authorName === "Elizabeth Funk")
      		{
      			team.elizabeth.issuesOpened++;
      		}


          if (data[i].state === "closed")
      		{
      			let closedName = data[i].closed_by.name;
      			if (closedName === "Amber Smith")
      			{
      				team.amber.issuesClosed++;
      			}
      			else if (closedName === "Jacob Kennedy")
      			{
      				team.jacob.issuesClosed++;
      			}
      			else if (closedName === "Joseph Xia")
      			{
      				team.joseph.issuesClosed++;
      			}
      			else if (closedName === "Karishma Patel")
      			{
      				team.karishma.issuesClosed++;
      			}
      			else if (closedName === "Elizabeth Funk")
      			{
      				team.elizabeth.issuesClosed++;
      			}

      			team.total.issuesClosed++;
			}
        team.total.issuesOpened++;


  }
  return team;
}

  async componentDidMount()
  {
    this.calculateUnitTests();
    //loops through json response to find number of commits
    for (var page = 1; page < 10; page++)
    {
        await axios.get(COMMITSURL + "&page=" + page).then(
          result => {
            let data = result.data;
            let team = this.calculateCommits(data);

            this.setState({
              isLoaded: true,
              team : team
            });
          },

          error => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        );
    }

    //loops through json response to find number of issues
    for (var pg = 1; pg < 10; pg++)
    {
        await axios.get(ISSUESURL + "&page=" + pg).then(
          result => {
            let data = result.data;
			let team = this.calculateIssues(data);

            this.setState({
              isLoaded: true,
              team : team
            });
          },

          error => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        );
    }
  }

  render()
  {
    const { error, isLoaded } = this.state;
    if (error)
    {
      return <div>Error: {error.message}</div>
    }
    else
    {
      return (
        <div>
         <br />

         <div className="pageBox" align="center">
         <br />
         <h2><b>Mission</b></h2>
         <p>Our mission is to provide information for people seeking to promote the rights of LGBT people in the USA. Understanding the issues faced by the LGBT community in America, and how our representatives feel on laws related to these issues will help people become better advocates for LGBT rights. Our site seeks to provide this information to the community and allies to help them work towards full LGBT equality.</p>
         <br />
         </div>

         <div className="pageBox" align="center">
         <br />
         <h2><b>Insight</b></h2>
         <p>When compiling the data about sponsorship of bills in relation to LGBT issues, a pattern began to emerge. There was a clear discrepancy between which of the two major parties in the US was likely to support legislation on issues relating to LGBT rights.</p>
         <br />
         </div>

         <div className="pageBox" align="center">
         <br />
         <h2><b>Group Members</b></h2>
         <br />

         <div className="row justify-content-center">
               <div className="col-lg-4 col-sm-6">
                 <div className="thumbnail">
                    <img src="img/elizabeth_profile.jpg" className="rounded mx-auto d-block" alt=""/>
                    <h4><b>Elizabeth Funk</b></h4>
                    <p><b>Bio:</b> I’m a junior computer science major. I enjoy archery and playing video games.<br />
         		 <b>Responsibilities:</b> API, Database<br /></p>

                 </div>
               </div>
               <div className="col-lg-4 col-sm-6">
                 <div className="thumbnail">
                    <img src="img/jacob_profile.jpg" className="rounded mx-auto d-block" alt=""/>
                    <h4><b>Jacob Kennedy</b></h4>
                    <p><b>Bio:</b> I’m a second year computer science major. I like to play music and watch Youtube. <br />
         			<b>Responsibilities:</b> API development <br /></p>

                 </div>
               </div>
               <div className="col-lg-4 col-sm-6">
                 <div className="thumbnail">
                    <img src="img/karishma_profile.jpg" className="rounded mx-auto d-block" alt=""/>
                    <h4><b>Karishma Patel</b></h4>
                    <p><b>Bio:</b> I’m a junior computer science major. I am in Texas Bluebonnets and enjoy kickboxing.<br />
         			<b>Responsibilities:</b> Website hosting/deploying and frontend<br /></p>
                 </div>
               </div>

         </div>

         <br /><br />

         <div className="row justify-content-center">
          <div className="col-lg-4 col-sm-6">
                 <div className="thumbnail">
                    <img src="img/amber_profile.jpg" className="rounded mx-auto d-block" alt=""/>
                    <h4><b>Amber Smith</b></h4>
                    <p><b>Bio:</b> I’m a 3rd year computer science major. I enjoy making video games, especially for VR!<br />
         			<b>Responsibilities:</b> Frontend <br /></p>

                 </div>
               </div>
               <div className="col-lg-4 col-sm-6">
                 <div className="thumbnail">
                    <img src="img/joseph_profile.jpg" className="rounded mx-auto d-block" alt=""/>
                    <h4><b>Joseph Xia</b></h4>
                    <p><b>Bio:</b> I’m a 3rd year computer science major and serial sleepy boy. I enjoy Pokemon, video games in general, and dogs. <br />
         			<b>Responsibilities:</b> Backend development, tech report writer <br /></p>
                 </div>
               </div>
         </div>
         <br />
         </div>

         <div className="pageBox" align="center">
         <br />
         <h2><b>GitLab Data</b></h2>
         <table style= {{width: "100%", textAlign:"center"}}>
           <tr>
             <th>Name</th>
             <th># commits</th>
             <th># opened issues</th>
             <th># closed issues</th>
             <th># unit tests</th>

           </tr>
           <tr>
             <td>Elizabeth Funk</td>
             <td>{this.state.team.elizabeth.commits}</td>
             <td>{this.state.team.elizabeth.issuesOpened}</td>
             <td>{this.state.team.elizabeth.issuesClosed}</td>
             <td>{this.state.team.elizabeth.unitTests}</td>
           </tr>
           <tr>
             <td>Jacob Kennedy</td>
             <td>{this.state.team.jacob.commits}</td>
             <td>{this.state.team.jacob.issuesOpened}</td>
             <td>{this.state.team.jacob.issuesClosed}</td>
             <td>{this.state.team.jacob.unitTests}</td>
           </tr>
           <tr>
             <td>Karishma Patel</td>
             <td>{this.state.team.karishma.commits}</td>
             <td>{this.state.team.karishma.issuesOpened}</td>
             <td>{this.state.team.karishma.issuesClosed}</td>
             <td>{this.state.team.karishma.unitTests}</td>
           </tr>
           <tr>
             <td>Amber Smith</td>
             <td>{this.state.team.amber.commits}</td>
             <td>{this.state.team.amber.issuesOpened}</td>
             <td>{this.state.team.amber.issuesClosed}</td>
             <td>{this.state.team.amber.unitTests}</td>
           </tr>
           <tr>
             <td>Joseph Xia</td>
             <td>{this.state.team.joseph.commits}</td>
             <td>{this.state.team.joseph.issuesOpened}</td>
             <td>{this.state.team.joseph.issuesClosed}</td>
             <td>{this.state.team.joseph.unitTests}</td>
           </tr>
         </table>
         <br />
         </div>

         <div className="pageBox" align="center">
         <br />
         <h2><b>Stats</b></h2>
         <p><b>Total Commits:</b> {this.state.team.total.commits}</p>
         <p><b>Total Issues:</b> {this.state.team.total.issuesOpened}</p>
         <p><b>Total Issues Closed:</b> {this.state.team.total.issuesClosed}</p>
         <p><b>Total Unit Tests:</b> {this.state.team.total.unitTests}</p>
         <br />
         </div>

         <div className="pageBox" align="center">
         <br />
         <h2><b>Data</b></h2>
         <p><a href="https://www.propublica.org/">Propublica</a></p>
         <p><a href="https://en.wikipedia.org/wiki/LGBT_rights_in_the_United_States">Wikipedia</a></p>
         <p><a href="https://github.com/unitedstates/images/tree/gh-pages/congress">Congress People Images on Github</a></p>
         <p><a href="https://www.hrc.org/state-maps">HRC</a></p>
         <br />
         </div>

         <div className="pageBox" align="center">
           <br />
           <h2><b>Tools</b></h2>

            <div className="modelRow">
            {formatTool("GitLab", "Used for version control", ".jpeg")}
            {formatTool("Slack", "Used for for communication")}
            {formatTool("AWS", "Used to host our website and API backend")}
            </div>

            <div className="modelRow">
            {formatTool("Postman", "Used to scrape our data from different API’s")}
            {formatTool("NameCheap", "Used to get the URL for our website")}
            {formatTool("S3 Bucket", "Used to host files for our static website")}
            </div>

            <div className="modelRow">
            {formatTool("Route 53", "Domain registration")}
            {formatTool("Cloudfront Distribution", "Used to optimize launching our website from the S3 bucket")}
            {formatTool("React", "Javascript framework")}
            </div>

            <div className="modelRow">
            {formatTool("Node", "Use modules to deploy application")}
            {formatTool("Bootstrap", "CSS framework")}
            {formatTool("Elastic Beanstalk", "Used to package our EC2 instance with our other resources and deploy our dynamic website")}
            </div>

            <div className="modelRow">
            {formatTool("Flask", "Used to create our API")}
            {formatTool("Python Requests", "Used to interface with other API’s and scrape data")}
            {formatTool("psycopg2", "Used to interface between python and the database to get data for the API")}
            </div>

            <div className="modelRow">
            {formatTool("Mocha", "Used to to create unit tests for our Javascript")}
            {formatTool("Chai", "Used to to create unit tests for our Javascript")}
            {formatTool("Enzyme", "Used to to create unit tests for our Javascript")}
            </div>

            <div className="modelRow">
            {formatTool("Selenium", "Used to test our site’s GUI")}
            {formatTool("Docker", "Container used for testing")}
            {formatTool("D3", "Library used for visualizations")}
            </div>
          </div>

          <div className="pageBox" align="center">
          <br />
          <h2><b>Links</b></h2>
          <p><a href="https://gitlab.com/JacobKennedy/idb-project/tree/master">Our Gitlab Repo</a></p>
          <p><a href="https://documenter.getpostman.com/view/6722847/S11LsHLd">Our API Doc</a></p>
          <br />
          </div>

        </div>
      );
    }
  }
}

export default About;
