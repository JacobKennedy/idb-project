import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import "./css/business-casual.css";
import { shortenDesc } from './ModelPage';

class Connections extends Component
{
    connectionCard (connection)
    {
        var title = "";

        if (this.props.model === "congress")
        {
            var support = connection.support ? "supports" : "does not support";
            var model = connection.model.substring(0, connection.model.length - 1);
            title = "This congress member " + support + " this " + model + ":";
        }
        else
        {
            if (connection.model === "congressMembers")
            {
                title = "Member of congress who supports this " + this.props.model + ":";
            }
            else
            if (connection.model === "bills")
            {
                title = "Related bill:"
            }
            else
            if (connection.model === "issues")
            {
                title = "Related issue:"
            }
        }

        return (
            <div>
            <div className="linkColumn">
              <div className="modelIcon-link">
              <div className="modelInfo">
              <Link to={"/" + connection.model + "/" + connection.name}>
                <img className= "modelThumbnail" src={connection.icon} alt="..."/>
              </Link>

              <h6><b>{title}</b></h6>
              <h6>{shortenDesc(connection.name, 50)}</h6>
              </div>
              </div>
            </div>
            </div>
        )
    }

    generateCards ()
    {
        let cards = []
        for (var i = 0; i < this.props.connections.length; i++)
        {
            cards.push(this.connectionCard(this.props.connections[i]));
        }

        return (
            <div>
            <div align="center" style= {{margin: "50px"}}>
              <div className="linkRow">
                {cards}
              </div>
            </div>
            </div>
        );
    }

    render ()
    {
        return (
            <div>
                {this.generateCards()}
            </div>
        );
    }
}

export default Connections;
