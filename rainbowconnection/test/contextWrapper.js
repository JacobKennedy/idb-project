import { BrowserRouter } from 'react-router-dom';
import { shape } from 'prop-types';

// This wrapper allows tests to include Router Links
// Source: https://github.com/airbnb/enzyme/issues/1112
// Instantiate router context
const router = {
  history: new BrowserRouter().history,
  route: {
    location: {},
    match: {},
  },
};

const createContext = () => ({
  context: { router },
  childContextTypes: { router: shape({}) },
});

export function mountWrap(node) {
  return mount(node, createContext());
}
