#gets all the member ids from the 115 congress
def main() :
	members = "./house.json"
	open("./house_ids.txt", "w").close()
	ids = open("./house_ids.txt", "a")
	num_results = -1
	num_ids = 0

	filein = open(members)
	for line in filein:
		if "\"num_results\":" in line :
			out = line.strip()
			out = out[15: 18]
			num_results = int(out)

		if "\"id\":" in line :
			out = line.strip()
			out = out[7: 14]
			ids.write(out+"\n")
			num_ids += 1
	ids.close()

	assert (num_results == num_ids)

if __name__ == "__main__":
    main()
