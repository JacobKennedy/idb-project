import json
import requests
import psycopg2

from connection import make_connection

beg_URL = "https://api.propublica.org/congress/v1/members/"
include_house = True
include_senate = True

def main() :
	#get_URLs()

	#senator test
	#print(get_member("https://api.propublica.org/congress/v1/members/C001098.json"))

	#house member test
	#print(get_member("https://api.propublica.org/congress/v1/members/A000374.json"))

	data = get_all_members()
	#data = {}
	database(data)


#gets the api urls for each member and prints them to a txt file
def get_URLs() :
	#senate
	if include_senate :
		house = "senator_ids.txt"
		out = "./senate_url.txt"
		open(out, "w").close()
		fileout = open(out, "a")	

		members = open(house)
		for person in members:
			person = person.strip()
			newURL = beg_URL + person
			newURL = newURL + ".json"
			fileout.write(newURL+"\n")

		fileout.close()

	#house
	if include_house :
		house = "house_ids.txt"
		out = "./house_url.txt"
		open(out, "w").close()
		fileout = open(out, "a")
		
		members = open(house)
		for person in members:
			person = person.strip()
			newURL = beg_URL + person
			newURL = newURL + ".json"
			fileout.write(newURL+"\n")

		fileout.close()

#returns a dict for the given member using the api url for that member
def get_member (url) :
	response = requests.get(url, headers = {'x-api-key' : 'xflvrD3HE27K9BpV0tZNOgSQzs9WuYNARJdpdAYe'})
	#print(response.status_code)

	data = response.json()

	data = data["results"][0]

	d = {}
	#d["f_name"] = data["first_name"]
	#d["l_name"] = data["last_name"]
	d["member_id"] = data["member_id"]
	d["name"] = data["first_name"] + " " + data["last_name"]
	d["url"] = data["url"]
	d["twitter"] = data["twitter_account"]
	d["facebook"] = data["facebook_account"]

	for role in data["roles"] :
		if role["congress"] == "115" :
			data115 = role

	d["chamber"] = data115["chamber"]
	d["title"] = data115["title"]
	d["state"] = data115["state"]
	d["party"] = data115["party"]
	d["office"] = data115["office"]
	d["phone"] = data115["phone"]
	d["fax"] = data115["fax"]
	d["contact_form"] = data115["contact_form"]

	committees = list()

	for entry in data115["committees"] :
		committees.append(entry["name"])

	d["committees"] = committees

	#print(d)
	return d


#returns a list of dicts where each dict is the database entry for a member
#returns all senators if include_senate is True and all house members if
#include_house = True
def get_all_members () :
	entries = list()
	#senate
	if include_senate :
		senators_file = "senate_url.txt"
		senators = open(senators_file)
		for member in senators :
			member_url = member.strip()
			entry = get_member(member_url)
			#print(entry)
			entries.append(entry)

		senators.close()
		#print(entries)
		#print(len(entries))

	#house
	if include_house :
		house_file = "house_url.txt"
		house = open(house_file)
		for member in house :
			member_url = member.strip()
			entry = get_member(member_url)
			#print(entry)
			entries.append(entry)
		house.close()
		#print(len(entries))

	return entries

#adds entries to the congress database using the psycopg2 executemany function
#data is a list of dicts where each dict is a single congress members table entry
def database(data) :
	connection = make_connection()

	cursor = connection.cursor()

	sql = "INSERT INTO congress (member_id, name, url, twitter, facebook, chamber, title, state, party, office, phone, fax, contact_form, committees) VALUES(%(member_id)s, %(name)s, %(url)s, %(twitter)s, %(facebook)s, %(chamber)s, %(title)s, %(state)s, %(party)s, %(office)s, %(phone)s, %(fax)s, %(contact_form)s, %(committees)s)"
	
	#test value for inserting a single row
	#data = {'member_id': 'C001098', 'name': 'Ted Cruz', 'url': 'https://www.cruz.senate.gov', 'twitter': 'SenTedCruz', 'facebook': 'SenatorTedCruz', 'chamber': 'Senate', 'title': 'Senator, 1st Class', 'state': 'TX', 'party': 'R', 'office': '404 Russell Senate Office Building', 'phone': '202-224-5922', 'fax': '202-228-3398', 'contact_form': 'http://www.cruz.senate.gov/?p=email_senator', 'committees': ['Joint Economic Committee', 'Committee on Armed Services', 'Committee on Commerce, Science, and Transportation', 'Committee on the Judiciary', 'Committee on Rules and Administration']}
	#cursor.execute(sql,data)

	cursor.executemany(sql, data)

	connection.commit()

	cursor.close()

	connection.close()

if __name__ == "__main__" :
    main()