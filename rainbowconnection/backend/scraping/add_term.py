import psycopg2
import re

from connection import make_connection


def main () :
	connection = make_connection()
	cursor = connection.cursor()
	get_sql = "SELECT name, title FROM congress"
	update_sql = "UPDATE congress SET term_end = %(term)s WHERE name = %(name)s"


	cursor.execute(get_sql)

	update_rows = list()

	row = cursor.fetchone()
	while row != None :
		name = row[0]
		title = row[1]

		if "Senator, 1st Class" in title :
			term = "2019"
		if "Senator, 2nd Class" in title :
			term = "2021"
		if "Senator, 3rd Class" in title :
			term = "2023"
		if "Representative" in title :
			term = "2019"
		if "Delegate" in title :
			term = "2019"

		
		row = {'term' : term, 'name' : name}
		
		update_rows.append(row)
		row = cursor.fetchone()

	cursor.executemany(update_sql, update_rows)
	connection.commit()

	cursor.close()
	connection.close()

	#print(update_rows)

if __name__ == "__main__" :
	main()
