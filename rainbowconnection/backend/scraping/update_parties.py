import psycopg2

from connection import make_connection

def main () :
	connection = make_connection()
	cursor = connection.cursor()
	get_sql = "SELECT name, party FROM congress"
	update_sql = "UPDATE congress SET party = %(party)s WHERE name = %(name)s"

	cursor.execute(get_sql)

	update_rows = list()

	row = cursor.fetchone()
	while row != None:
		name = row[0]
		party = row[1]
		if party == 'R' :
			party = 'Republican'
		elif party == 'D':
			party = 'Democrat'
		elif party == 'I':
			party = 'Independent'
		newRow = {'party': party,'name': name}
		update_rows.append(newRow)
		row = cursor.fetchone()

	cursor.executemany(update_sql, update_rows)
	connection.commit()

	cursor.close()
	connection.close()


if __name__ == "__main__" :
	main()