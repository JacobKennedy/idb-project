import json
import requests
import psycopg2

from connection import make_connection

beg_URL = "https://api.propublica.org/congress/v1/115/bills/"
co_url = "/cosponsors.json"

def main() :
	#data = get_bill("https://api.propublica.org/congress/v1/115/bills/s411.json", "https://api.propublica.org/congress/v1/115/bills/s411/cosponsors.json")
	#print(data)
	#types(data)

	data = get_all_bills()
	#print(data)
	database(data)


#debugging, check types returned from json
def types(data) :
	for key, value in data.items() :
		print(key)
		print(type(value))

	

#returns a dict for the given bill using the api url for that bill
def get_bill (bill_url, sponsor_url) :
	bill_response = requests.get(bill_url, headers = {'x-api-key' : 'xflvrD3HE27K9BpV0tZNOgSQzs9WuYNARJdpdAYe'})
	sponsor_response = requests.get(sponsor_url, headers = {'x-api-key' : 'xflvrD3HE27K9BpV0tZNOgSQzs9WuYNARJdpdAYe'})

	#print(response.status_code)

	data = bill_response.json()
	data = data["results"][0]
	#print(data)

	d = {}

	d["bill_id"] = data["bill_id"]
	d["number"] = data["number"]
	d["name"] = data["title"]
	d["intro_date"] = data["introduced_date"]
	d["sponsor"] = data["sponsor"]
	d["summary"] = data["summary"]
	d["link"] = data["congressdotgov_url"]

	#cosponsors by party
	d["d"] = 0
	d["r"] = 0
	d["i"] = 0
	for party, num in data["cosponsors_by_party"].items() :
		d[party.lower()] = num

	#sponsors
	data = sponsor_response.json()
	data = data["results"][0]

	sponsors = list()

	for item in data["cosponsors"] :
		sponsors.append(item["name"])

	d["cosponsors"] = sponsors


	#print(d)
	return d


#returns a list of dicts where each dict is the database entry for a bill
def get_all_bills () :
	entries = list()
	bills = open("./bill_ids.txt")

	for bill in bills :
		bill = bill.strip()
		bill_url = beg_URL + bill + ".json"
		sponsor_url = beg_URL + bill + co_url
		entries.append(get_bill(bill_url, sponsor_url))

	return entries

#adds entries to the congress database using the psycopg2 executemany function
#data is a list of dicts where each dict is a bill table entry
def database(data) :
	connection = make_connection()

	cursor = connection.cursor()

	sql = "INSERT INTO bills (bill_id, name, number, intro_date, sponsor, summary, d, r, i, cosponsors, link) VALUES(%(bill_id)s, %(name)s, %(number)s, %(intro_date)s, %(sponsor)s, %(summary)s, %(d)s, %(r)s, %(i)s,%(cosponsors)s, %(link)s)"

	cursor.executemany(sql, data)

	connection.commit()

	cursor.close()

	connection.close()

if __name__ == "__main__" :
    main()