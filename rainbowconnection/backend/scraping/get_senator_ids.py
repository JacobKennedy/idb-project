#gets all the member ids from the 115 congress
import requests
import json



def main() :
	response = requests.get("https://api.propublica.org/congress/v1/115/senate/members.json", headers = {'x-api-key' : 'xflvrD3HE27K9BpV0tZNOgSQzs9WuYNARJdpdAYe'})
	sen_json = open("./senators.json", "w")
	sen_json.write(response.text)
	sen_json.close
	
	members = "./senators.json"
	#clear file
	open("./senator_ids.txt", "w").close()
	ids = open("./senator_ids.txt", "a")
	num_results = -1
	num_ids = 0
	filein = open(members)
	for line in filein:
		if "\"num_results\":" in line :
			out = line.strip()
			out = out[15: 18]
			num_results = int(out)
		if "\"id\":" in line :
			out = line.strip()
			out = out[7: 14]
			ids.write(out+"\n")
			num_ids += 1
		#ids.write(line + "END LINE\n")
	ids.close()

	assert (num_results == num_ids)

if __name__ == "__main__":
    main()

