import psycopg2
import re

from connection import make_connection


def main () :
	connection = make_connection()
	cursor = connection.cursor()
	get_sql = "SELECT bill_id, sponsor, cosponsors FROM bills"
	update_sql = "UPDATE bills SET sponsor = %(sponsor)s, cosponsors = %(cospon)s WHERE bill_id = %(b_id)s"


	cursor.execute(get_sql)

	update_rows = list()

	row = cursor.fetchone()
	while row != None :
		bill = row[0]
		sponsor = row[1]
		cospon = row[2]

		if "." in sponsor :
			sponsor = re.sub("|.\. ", "", sponsor)

		new_cospon = list()

		for person in cospon :
			if "." in person :
				person = re.sub("|.\. ", "", person)
	
			new_cospon.append(person)

		
		row = {'b_id' : bill, 'sponsor' : sponsor, 'cospon' : new_cospon}
		
		update_rows.append(row)
		row = cursor.fetchone()

	cursor.executemany(update_sql, update_rows)
	connection.commit()

	cursor.close()
	connection.close()

	#print(update_rows)

if __name__ == "__main__" :
	main()
