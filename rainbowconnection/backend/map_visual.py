import psycopg2
import requests
import json

from connection import make_connection


def main():
    URL = "https://api.rainbowconnection.me/v1/bills/search?sponsor="
    num_senate_bills = 15
    num_house_bills = 18
    sen_response = requests.get(
        "https://api.rainbowconnection.me/v1/congress/search?chamber=Senate"
    ).json()
    house_response = requests.get(
        "https://api.rainbowconnection.me/v1/congress/search?chamber=House"
    ).json()
    states = [
        "AL",
        "AK",
        "AZ",
        "AR",
        "CA",
        "CO",
        "CT",
        "DE",
        "FL",
        "GA",
        "HI",
        "ID",
        "IL",
        "IN",
        "IA",
        "KS",
        "KY",
        "LA",
        "ME",
        "MD",
        "MA",
        "MI",
        "MN",
        "MS",
        "MO",
        "MT",
        "NE",
        "NV",
        "NH",
        "NJ",
        "NM",
        "NY",
        "NC",
        "ND",
        "OH",
        "OK",
        "OR",
        "PA",
        "RI",
        "SC",
        "SD",
        "TN",
        "TX",
        "UT",
        "VT",
        "VA",
        "WA",
        "WV",
        "WI",
        "WY",
    ]
    result = dict()

    for state in states:
        num_sen = 0
        senate_bills_support = 0
        num_house = 0
        house_bills_support = 0
        # print(state)

        # print('Senate')
        for entry in sen_response:
            if state == entry["state"]:
                num_sen += 1
                # print(entry['name'])
                senate_bills_support += len(requests.get(URL + entry["name"]).json())
                # count how many bills sponsored by this senator

                # print('House')
        for entry in house_response:
            if state == entry["state"]:
                num_house += 1
                # print(entry['name'])
                house_bills_support += len(requests.get(URL + entry["name"]).json())

        num_sen *= num_senate_bills
        num_house *= num_house_bills
        # 				divide _bills_support by num_ for % supported
        result[state] = (senate_bills_support, num_sen, house_bills_support, num_house)

    print(result)
    return result


if __name__ == "__main__":
    main()
