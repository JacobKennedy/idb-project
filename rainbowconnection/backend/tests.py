from unittest import main, TestCase
from application import *


class MyUnitTests(TestCase):
    def setUp(self):
        application.config["TESTING"] = True
        application.config["WTF_CSRF_ENABLED"] = False
        application.config["DEBUG"] = False
        self.application = application.test_client()

    def test_home(self):
        response = self.application.get("/", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_congress(self):
        response = self.application.get("/v1/congress", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_congress_name(self):
        response = self.application.get(
            "/v1/congress?name=Ted Cruz", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_congress_name2(self):
        response = self.application.get(
            "/v1/congress?name=Tammy Baldwin", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_congress_all(self):
        response = self.application.get("/v1/congress/all", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_issues(self):
        response = self.application.get("/v1/issues", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_issues_name(self):
        response = self.application.get(
            "/v1/issues?name=Housing", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_issues_all(self):
        response = self.application.get("/v1/issues/all", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_bils(self):
        response = self.application.get("/v1/bills", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_bills_name(self):
        response = self.application.get("/v1/bills?name=S.411", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_bills_name2(self):
        response = self.application.get(
            "/v1/bills?name=H.R.1498", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_bills_all(self):
        response = self.application.get("/v1/bills/all", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_congress_search1(self):
        response = self.application.get("/v1/congress/search", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_congress_search2(self):
        response = self.application.get(
            "/v1/congress/search?phrase=WI", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_congress_search3(self):
        response = self.application.get(
            "/v1/congress/search?name=Tammy", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_congress_search4(self):
        response = self.application.get(
            "/v1/congress/search?phrase=WI&name=Tammy", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_congress_search5(self):
        response = self.application.get(
            "/v1/congress/search?phrase=WI&sort_by=name", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_congress_search6(self):
        response = self.application.get(
            "/v1/congress/search?phrase=WI&sort_by=name&desc=True",
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)

    def test_issues_search1(self):
        response = self.application.get("/v1/issues/search", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_issues_search2(self):
        response = self.application.get(
            "/v1/issues/search?phrase=ing", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_issues_search3(self):
        response = self.application.get(
            "/v1/issues/search?numStates=22", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_issues_search4(self):
        response = self.application.get(
            "/v1/issues/search?phrase=ing&numStates=22", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_issues_search5(self):
        response = self.application.get(
            "/v1/issues/search?phrase=ing&sort_by=name", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_issues_search6(self):
        response = self.application.get(
            "/v1/issues/search?phrase=ing&sort_by=name&desc=True", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_bills_search1(self):
        response = self.application.get("/v1/bills/search", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_bills_search2(self):
        response = self.application.get(
            "/v1/bills/search?phrase=H.R.", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_bills_search3(self):
        response = self.application.get(
            "/v1/bills/search?date=2017", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_bills_search4(self):
        response = self.application.get(
            "/v1/bills/search?phrase=H.R.&date=2017", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_bills_search5(self):
        response = self.application.get(
            "/v1/bills/search?phrase=H.R.&sort_by=num", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

    def test_bills_search6(self):
        response = self.application.get(
            "/v1/bills/search?phrase=H.R.&sort_by=num&desc=True", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":  # pragma: no cover
    main()
