import flask
import psycopg2
from flask import request, jsonify
from flask_cors import CORS, cross_origin

app = flask.Flask(__name__)
cors = CORS(app)
app.config["DEBUG"] = True
app.config["CORS_HEADERS"] = "Content-Type"

issues = []
try:
    connection = psycopg2.connect(
        user="rainbowconnection",
        password="SWEisdeath",
        host="rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com",
        port="5432",
        database="congress",
    )

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM issues")

    row = cursor.fetchone()

    while row != None:

        name = row[0]
        if name == None:
            name = "N/A"

        fedProtect = row[6]
        if fedProtect == None:
            fedProtect = "N/A"

        date = row[1]
        if date == None:
            date = "N/A"

        numStates = row[7]
        if numStates == None:
            numStates = "N/A"

        desc = row[2]
        if desc == None:
            desc = "N/A"

        icon = row[3]
        if icon == None:
            icon = "N/A"

        map = row[4]
        if map == None:
            map = "N/A"

        bills = row[5]

        connections = [
            {"model": "bills", "name": bills[0], "icon": "default"},
            {
                "model": "congressMembers",
                "name": "Tammy Baldwin",
                "icon": "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/B001230.jpg",
            },
        ]

        issues.append(
            {
                "name": name,
                "fedProtect": fedProtect,
                "date": date,
                "numStates": numStates,
                "desc": desc,
                "icon": icon,
                "map": map,
                "connections": connections,
                "bills": bills,
            }
        )

        row = cursor.fetchone()

    cursor.close()

    connection.close()

except (psycopg2.DatabaseError, e):
    if connection:
        connection.rollback()


bills = []
try:
    connection = psycopg2.connect(
        user="rainbowconnection",
        password="SWEisdeath",
        host="rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com",
        port="5432",
        database="congress",
    )

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM bills")

    row = cursor.fetchone()

    while row != None:

        name = row[1]
        if name == None:
            name = "N/A"

        num = row[7]
        if num == None:
            num = "N/A"

        date = row[2]
        if date == None:
            date = "N/A"

        sponsor = row[3]
        if sponsor == None:
            sponsor = "N/A"

        link = row[6]
        if link == None:
            link = "N/A"

        summary = row[4]
        if summary == None:
            summary = "N/A"

        dems = row[9]
        reps = row[8]
        ind = row[10]

        partyData = []
        partyData += ["Democrats", dems]
        partyData += ["Republicans", reps]
        partyData += ["Independents", ind]

        cosponsors = row[5]

        issuename = "Error: issue not found"
        photo = "Error"
        for issue in issues:
            bs = issue.get("bills")
            if num in bs:
                issuename = issue.get("name")
                photo = issue.get("icon")
                break

        connections = [
            {"model": "congressMembers", "name": sponsor, "icon": "test.jpg"},
            {"model": "issues", "name": issuename, "icon": photo},
        ]

        bills.append(
            {
                "name": name,
                "num": num,
                "date": date,
                "sponsor": sponsor,
                "link": link,
                "summary": summary,
                "partyData": partyData,
                "cosponsors": cosponsors,
                "connections": connections,
                "picture": "default",
            }
        )

        row = cursor.fetchone()

    cursor.close()

    connection.close()
except (psycopg2.DatabaseError, e):
    if connection:
        connection.rollback()

congress = []

try:
    connection = psycopg2.connect(
        user="rainbowconnection",
        password="SWEisdeath",
        host="rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com",
        port="5432",
        database="congress",
    )

    cursor = connection.cursor()
    cursor.execute("SELECT * FROM congress")

    row = cursor.fetchone()

    while row != None:

        name = row[0]
        if name == None:
            name = "N/A"
        title = row[5]
        if title == None:
            title = "N/A"
        party = row[7]
        if party == None:
            party = "N/A"
        fax = row[8]
        if fax == None:
            fax = "N/A"
        phone = row[9]
        if phone == None:
            phone = "N/A"
        office = row[10]
        if office == None:
            office = "N/A"
        contact = row[11]
        if contact == None:
            contact = "N/A"
        id = row[13]
        state = row[6]
        termEnd = "N/A"
        if title == "Representative":
            termEnd = "2019"
        if title == "Senator, 1st Class":
            termEnd = "2019"
        if title == "Senator, 2nd Class":
            termEnd = "2021"
        if title == "Senator, 3rd Class":
            termEnd = "2023"

        committees = row[12]

        memberid = row[13]

        chamber = row[4]

        link = row[1]

        twitter = row[2]
        linktwitter = "N/A"
        if twitter == None:
            twitter = "N/A"
        else:
            linktwitter = "https://twitter.com/" + twitter + "?lang=en"

        facebook = row[3]
        linkfacebook = "N/A"
        if facebook == None:
            facebook = "N/A"
        else:
            linkfacebook = "https://www.facebook.com/" + facebook + "/"

        linktwitter = "https://twitter.com/" + twitter + "?lang=en"

        b = "S.411"
        support = "False"

        i = "Hate Crimes"
        supporti = "False"

        photo = "http://blog.nohatespeechmovement.org/wp-content/uploads/2014/07/half-vic-lgbt.jpg"

        for bill in bills:
            csp = bill.get("cosponsors")
            if name in csp:
                b = bill.get("num")
                support = "True"
                i = bill.get("connections")[1].get("name")
                supporti = "True"
                photo = bill.get("connections")[1].get("icon")
                break

        connections = [b, support, "default", i, supporti, photo]

        toAdd = {
            "name": name,
            "title": title,
            "party": party,
            "phone": phone,
            "office": office,
            "contact": contact,
            "icon": "https://raw.githubusercontent.com/unitedstates/images/gh-pages/congress/225x275/"
            + id
            + ".jpg",
            "map": "https://maps.googleapis.com/maps/api/staticmap?center="
            + state
            + "&zoom=5&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"
            + state
            + "&key=AIzaSyDkjJLWEtXMs8dSDfEMGUHyXh1a5GMs0d0",
            "state": state,
            "termEnd": termEnd,
            "committess": committees,
            "senatelink": link,
            "twitter": twitter,
            "facebook": facebook,
            "linktwitter": linktwitter,
            "linkfacebook": linkfacebook,
            "connections": connections,
            "chamber": chamber,
            "fax": fax,
            "memberid": memberid,
        }
        congress.append(toAdd)
        row = cursor.fetchone()

    cursor.close()

    connection.close()
except (psycopg2.DatabaseError, e):
    if connection:
        connection.rollback()

try:
    connection = psycopg2.connect(
        user="rainbowconnection",
        password="SWEisdeath",
        host="rainbow.cjqoruxzpifj.us-east-2.rds.amazonaws.com",
        port="5432",
        database="congress",
    )

    cursor = connection.cursor()

    toAdd = list()

    for x in congress:
        id = x["memberid"]
        c = x["connections"]
        row = {"c": c, "id": id}
        toAdd.append(row)
        # print ("UPDATE congress SET connections=%s WHERE memberid=%s;"%(c, id))
        # cursor.execute("UPDATE Congress SET Connections = %(c)s WHERE Memberid = %(id)s;", (c, id))
    print(toAdd[0]["c"])

    cursor.executemany(
        "UPDATE congress SET connections= %(c)s WHERE memberid= %(id)s;", toAdd
    )

    connection.commit()
    cursor.close()
    connection.close()
except (psycopg2.DatabaseError, e):
    print(e.pgcode)
    if connection:
        connection.rollback()
