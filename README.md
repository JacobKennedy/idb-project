Members:

Karishma Patel
EID: kmp3555
GitLab ID: karishma946
Phase 1:
estimated completion time: (hours: 20)
actual completion time: (hours: 30)
Phase 2:
estimated completion time: (hours:40)
actual completion time: (hours: 35)
Phase 3:
estimated completion time: (hours:30)
actual completion time: (hours: 30)
Phase 4:
estimated completion time: (hours:25)
actual completion time: (hours: 25)

Joseph Xia
EID: jx2598
GitLab ID: joexia
Phase 1:
estimated completion time: (hours: 30)
actual completion time: (hours: 25)
Phase 2:
estimated completion time: (hours: 40)
actual completion time: (hours: 35)
Phase 3:
estimated completion time: (hours:30)
actual completion time: (hours: 30)
Phase 4:
estimated completion time: (hours:25)
actual completion time: (hours: 25)


Jacob Kennedy
EID: jak3895
GitLab ID: JacobKennedy
Phase 1:
estimated completion time: (hours: 25)
actual completion time: (hours: 25)
Phase 2:
estimated completion time: (hours: 40)
actual completion time: (hours: 35)
Phase 3:
estimated completion time: (hours:30)
actual completion time: (hours: 30)
Phase 4:
estimated completion time: (hours:25)
actual completion time: (hours: 25)


Elizabeth Funk
EID: enf353
GitLab ID: elizabeth_funk
Phase 1:
estimated completion time: (hours: 21)
actual completion time: (hours: 25)
Phase 2:
estimated completion time: (hours: 30)
actual completion time: (hours: 35)
Phase 3:
estimated completion time: (hours:30)
actual completion time: (hours: 30)
Phase 4:
estimated completion time: (hours:25)
actual completion time: (hours: 25)


Amber Smith
EID: as75287
GitLab ID: as75287
Phase 1:
estimated completion time: (hours: 15)
actual completion time: (hours: 25)
Phase 2:
estimated completion time: (hours: 30)
actual completion time: (hours: 35)
Phase 3:
estimated completion time: (hours:30)
actual completion time: (hours: 30)
Phase 4:
estimated completion time: (hours:25)
actual completion time: (hours: 25)


Git SHA: e01ed04cd3e1909c4d82bded9b38a2cf7c8ad21d
link to GitLab pipelines: https://gitlab.com/JacobKennedy/idb-project/pipelines
link to website: https://www.rainbowconnection.me/
link to api: https://api.rainbowconnection.me/
link to our visualizations: https://rainbowconnection.me/visualizations
comments: Our main.py is actually named application.py. It's located in master/rainbowconnection/application.py
